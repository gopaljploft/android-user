package com.cab_taxi.infinitecabs;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.VideoView;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Background_Service.StatusManager_Background_Store_Data;
import com.cab_taxi.infinitecabs.Extra.ScrollingImageView;
import com.cab_taxi.infinitecabs.Extra.YoYo;
import com.cab_taxi.infinitecabs.Model.User_Status_Background_Model;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {

    /**
     * Duration of wait
     **/
    private final int SPLASH_DISPLAY_LENGTH = 2000;
    private YoYo.YoYoString rope;
    private YoYo.YoYoString rope1;
    UserSessionManager session;
    public static final String USER_LOGIN_DATA = "User Login Data";
    public static final String USER_BOOKING_STATUS = "User Status Data";
    SharedPreferences.Editor editor;
    StatusManager_Background_Store_Data statusManager_background_store_data;
    HashMap<String, String> background_store_data;
    VideoView videoView;
    /**
     * Called when the activity is first created.
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_splash_screen);
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.black_text_color));
        editor= getSharedPreferences(USER_LOGIN_DATA, MODE_PRIVATE).edit();
       // editor= getSharedPreferences(USER_BOOKING_STATUS, MODE_PRIVATE).edit();
//        statusManager_background_store_data = new StatusManager_Background_Store_Data(getApplicationContext());
//        background_store_data = statusManager_background_store_data.getUserDetails();

/*        final ImageView logo = findViewById(R.id.iv_logo);
        final ImageView name = findViewById(R.id.iv_name);
        final CardView cardlogo = findViewById(R.id.cardlogo);
        final LinearLayout llayout = findViewById(R.id.llayout);
        name.setVisibility(View.GONE);


        AlphaAnimation fadeIn = new AlphaAnimation(0, 1);

        AlphaAnimation fadeOut = new AlphaAnimation(1, 0);


        final AnimationSet set = new AnimationSet(false);

        set.addAnimation(fadeIn);
        fadeOut.setStartOffset(2000);
        set.setDuration(2000);
        cardlogo.startAnimation(set);

        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                imageView.startAnimation(set);

                if (rope != null) {
                    rope.stop(true);
                }

                name.setVisibility(View.VISIBLE);
                rope = YoYo.with(Techniques.FlipInX)
                        .duration(2000)
                        .repeat(0)
                        .pivot(YoYo.CENTER_PIVOT, YoYo.CENTER_PIVOT)
                        .interpolate(new AccelerateDecelerateInterpolator())
                        .withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                Intent mainIntent = new Intent(SplashScreen.this, Login.class);
                                SplashScreen.this.startActivity(mainIntent);
                                SplashScreen.this.finish();
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {
                                Toast.makeText(SplashScreen.this, "canceled previous animation", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        })
                        .playOn(name);

            }
        });*/



        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                 // Create an Intent that will start the Menu-Activity.
                session = new UserSessionManager(getApplicationContext());
                session.checkLogin();
                finish();
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);

             }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void jump() {
        if (isFinishing())
            return;
        session = new UserSessionManager(getApplicationContext());
        session.checkLogin();
        finish();
    }
    private void User_Status_Check_Background_Store(Map< String, String > map){
        Call<User_Status_Background_Model> call = Apis.getAPIService().getUserStatus(map);
        call.enqueue(new Callback<User_Status_Background_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<User_Status_Background_Model> call, Response<User_Status_Background_Model> response) {
                // dialog.dismiss();
                final User_Status_Background_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                        switch (user.getBookingData().getStatus()) {
                            case "Accepted":
                                statusManager_background_store_data.iseditor();
                                statusManager_background_store_data.createUserLoginSession(String.valueOf(user.getBookingData().getBookingId()),
                                        user.getBookingData().getPickupLocation(), user.getBookingData().getDropLocation(),
                                        user.getBookingData().getDriverName(), user.getBookingData().getDriverId(), user.getBookingData().getDriverMobile(),
                                        user.getBookingData().getDriverImage(), user.getBookingData().getBookingdate(), user.getBookingData().getPickuplat(),
                                        user.getBookingData().getPickuplng(), user.getBookingData().getDroplat(), user.getBookingData().getDroplng(), user.getBookingData().getStatus()
                                        , user.getBookingData().getDriverEmail(), user.getBookingData().getDriverlicense_no(), user.getBookingData().getDrivercar_type(), user.getBookingData().getDriverCar_Model(),
                                        user.getBookingData().getUserstarrating());
                                break;

                        }

                    }


                    else {
                        statusManager_background_store_data.iseditor();
                        statusManager_background_store_data.createUserLoginSession("",
                                "", "",
                                "", "", "",
                                "", "", "",
                                "", "", "", ""
                                ,"",
                                "", "",
                                "", "");
                    }

                }
            }

            @Override
            public void onFailure(Call<User_Status_Background_Model> call, Throwable t) {
                //dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
}
