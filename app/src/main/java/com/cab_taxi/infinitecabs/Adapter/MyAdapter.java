package com.cab_taxi.infinitecabs.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.cab_taxi.infinitecabs.R;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
    /*  final MyListData myListData = listdata[position];
        holder.tv_serviceType.setText(listdata[position].getDescription());
        holder.tv_passengerCount.setText(listdata[position].getDescription());
        holder.tv_carfare.setText(listdata[position].getDescription());
        holder.iv_car.setImageResource(listdata[position].getImgId());
        holder.listItem_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "click on item: " + myListData.getDescription(), Toast.LENGTH_LONG).show();
            }
        });
        */
    }


    @Override
    public int getItemCount() {
        return 4;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView iv_car;
        public TextView tv_serviceType, tv_passengerCount, tv_carfare;
        public RelativeLayout listItem_layout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.iv_car = itemView.findViewById(R.id.iv_car);
            this.tv_serviceType = itemView.findViewById(R.id.tv_serviceType);
            this.tv_passengerCount = itemView.findViewById(R.id.tv_passengerCount);
            this.tv_carfare = itemView.findViewById(R.id.tv_carfare);
            listItem_layout = itemView.findViewById(R.id.listItem_layout);
        }
    }
}