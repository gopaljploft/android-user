package com.cab_taxi.infinitecabs.Extra;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


public class Utility {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    public static SharedPreferences getPref(Context context) {
        return context.getSharedPreferences("LalsAcademy", Context.MODE_PRIVATE);
    }

    public static boolean isLogin(Context context) {
        return getPref(context).getBoolean("isalwaysLogin", false);
    }

    public static void saveNumber(Context context, String number) {
        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putString("number", number);
        // editor.putBoolean("isalwaysLogin", true);
        editor.apply();
    }
    public static void saveUserID(Context context, String number) {
        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putString("user_id", number);
        // editor.putBoolean("isalwaysLogin", true);
        editor.apply();
    }

    public static void latlng(Context context, Double lat, Double lng) {
        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putString("lat", String.valueOf(lat));
        editor.putString("lng", String.valueOf(lng));

        editor.apply();
    }
  public static void setUrl(Context context, String lat) {
        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putString("url", String.valueOf(lat));
        editor.apply();
    }

    public static String getlat(Context context) {
        return getPref(context).getString("lat", "0.0");
    }

    public static String getURL(Context context) {
        return getPref(context).getString("url", "0");
    }

    public static String getCard(Context context) {
        return getPref(context).getString("card", "0");
    }
    public static String getPaytype(Context context) {
        return getPref(context).getString("paytype", "0");
    }
    public static void setCard(Context context, String card, String paytype) {
        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putString("card",card );
        editor.putString("paytype", paytype);
        editor.apply();
    }

    public static String getlng(Context context) {
        return getPref(context).getString("lng", "0.0");
    }

    public static String getpic(Context context) {
        return getPref(context).getString("url", "");
    }


    public static void logout(Context context) {
        getPref(context).edit().clear().apply();
    }

    public static String getLoginNumber(Context context) {
        return getPref(context).getString("number", "xxxxxxxxxx");
    }

    public static String getLoginId(Context context) {
        return getPref(context).getString("user_id", "");
    }

    public static String getLoginFullName(Context context) {
        return getPref(context).getString("fullname", "");
    }


    public static String getLoginEmail(Context context) {
        return getPref(context).getString("email", "");
    }


    public static String getdob(Context context) {
        return getPref(context).getString("dob", "");
    }


    public static String getLoginPhone(Context context) {
        return getPref(context).getString("number", "");
    }

    public static String getstate(Context context) {
        return getPref(context).getString("state", "");
    }

    public static String getGenter(Context context) {
        return getPref(context).getString("gender", "");
    }

    public static String getreligion(Context context) {
        return getPref(context).getString("religion", "");
    }

    public static String getMotherTongue(Context context) {
        return getPref(context).getString("MotherTongue", "");
    }

    public static String email(Context context) {
        return getPref(context).getString("email", "");
    }


  /*  public static void setDashBoardData(Context context, LoginModel.UserdataBean data) {

        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putString("email", data.getEmail());
        editor.putString("fname", data.getFname());
        editor.putString("lname", data.getLname());
        editor.putString("gender", data.getGender());
        editor.putString("dob", data.getDob());
        editor.putString("phone", data.getPhone());
        editor.putString("state", data.getState());
        editor.putString("religion", data.getReligion());
        editor.putString("MotherTongue", data.getMotherTongue());
        editor.apply();

    }*/


    public static void setlogin(Context context) {

        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putBoolean("isalwaysLogin", true);
        editor.apply();

    }
    public static void setloginID(Context context, String id) {

        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putString("user_id", id);
        editor.apply();

    }

    public static void setdetail(Context context, String image) {

        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putString("profile_image", image);
        editor.apply();

    }
    public static void setCountryCode(Context context, String code) {
        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putString("CountryCode", code);
        editor.apply();
    }
    public static String getCountryCode(Context context) {
        return getPref(context).getString("CountryCode", "");
    }

    public static String getUserName(Context context) {
        return getPref(context).getString("name", "");
    }

    public static String getProfilePic(Context context) {
        return getPref(context).getString("profile_image", "");
    }


    public static void setlogout(Context context) {

        SharedPreferences.Editor editor = getPref(context).edit();
        editor.putBoolean("isalwaysLogin", false);
        editor.apply();

    }

    public static void showspinner(Context context, final String[] items, final EditText editText, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select " + title);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                editText.setText(items[item]);
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void showspinner(Context context, final String[] items, final EditText editText, final EditText editText1, String title) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select " + title);
        final String[] selectedarray = new String[items.length];
        builder.setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                if (isChecked){
                    selectedarray[which] = items[which];
                }else{
                    selectedarray[which] = "@";
                }
            }
        });
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String abc = Arrays.toString(selectedarray);
                abc = abc.replace("[","");
                abc = abc.replace("]","");
                abc = abc.replace(" ","");
                abc = abc.replace(",,",",");
                abc = abc.replace(",null","");
                abc = abc.replace("null,","");
                abc = abc.replace(",@","");
                abc = abc.replace("@,","");
                abc = abc.replace("@","");
                editText.setText(abc);
            }
        });
        builder.show();
    }
    public static void showspinner123(Context context, final String[] items, final EditText editText, final EditText editText1, String title, Boolean abc) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select " + title);
        final String[] selectedarray = new String[items.length];
        builder.setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                if (isChecked){
                    selectedarray[which] = items[which];
                }else{
                    selectedarray[which] = "@";
                }
            }
        });
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String abc = Arrays.toString(selectedarray);
                abc = abc.replace("[","");
                abc = abc.replace("]","");
                abc = abc.replace(" ","");
                abc = abc.replace(",,",",");
                abc = abc.replace(",null","");
                abc = abc.replace("null,","");
                abc = abc.replace(",@","");
                abc = abc.replace("@,","");
                abc = abc.replace("@","");
                editText.setText(abc);
            }
        });
        builder.show();
    }

    public static void showspinner1(Context context, final String[] items, final EditText editText, final EditText editText1, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select " + title);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                editText.setText(items[item]);
                if (items[item].equals("India")) {
                    editText1.setText("");
                    editText1.setVisibility(View.VISIBLE);
                } else {
                    editText1.setVisibility(View.GONE);
                }

                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void showspinner1(Context context, final String[] items, final TextView editText, final TextView editText1, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select " + title);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                editText.setText(items[item]);
                if (items[item].equals("India")) {
                    editText1.setText("");
                    editText1.setVisibility(View.VISIBLE);
                } else {
                    editText1.setVisibility(View.GONE);
                }

                dialog.dismiss();
            }
        });
        builder.show();
    }


    public static void showspinner(Context context, final String[] items, final TextView editText, final TextView editText1, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select " + title);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                editText.setText(items[item]);
                if (item < 5) {
                    editText1.setText("");
                    editText1.setVisibility(View.VISIBLE);
                } else {
                    editText1.setVisibility(View.GONE);
                }
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void showspinner(Context context, final String[] items, final TextView editText, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select " + title);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                editText.setText(items[item]);
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void multichoiceitem(Context context, final String[] items, final EditText editText, String title) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select " + title);
        final String[] selectedarray = new String[items.length];
        builder.setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                if (isChecked){
                    selectedarray[which] = items[which];
                }else{
                    selectedarray[which] = "@";
                }
            }
        });
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String abc = Arrays.toString(selectedarray);
                abc = abc.replace("[","");
                abc = abc.replace("]","");
                abc = abc.replace(" ","");
                abc = abc.replace(",,",",");
                abc = abc.replace(",null","");
                abc = abc.replace("null,","");
                abc = abc.replace(",@","");
                abc = abc.replace("@,","");
                abc = abc.replace("@","");
                editText.setText(abc);
            }
        });
        builder.show();
    }
    public static void multichoiceitem(Context context, final String[] items, final EditText editText, final EditText editText1, String title) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select " + title);
        final String[] selectedarray = new String[items.length];
        builder.setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                if (isChecked){
                    selectedarray[which] = items[which];
                }else{
                    selectedarray[which] = "@";
                }
            }
        });
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String abc = Arrays.toString(selectedarray);
                abc = abc.replace("[","");
                abc = abc.replace("]","");
                abc = abc.replace(" ","");
                abc = abc.replace(",,",",");
                abc = abc.replace(",null","");
                abc = abc.replace("null,","");
                abc = abc.replace(",@","");
                abc = abc.replace("@,","");
                abc = abc.replace("@","");


                if (abc.contains("India")){
                    editText1.setVisibility(View.VISIBLE);
                    editText.setText(abc);
                }
                else{
                    editText1.setVisibility(View.GONE);
                    editText.setText(abc);
                }

            }
        });
        builder.show();
    }
    public static void  multichoiceitem3(Context context, final String[] items, final EditText editText, String title) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select " + title);
        final String[] selectedarray = new String[items.length];
        builder.setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                if (isChecked){
                    selectedarray[which] = items[which];
                }else{
                    selectedarray[which] = "@";
                }
            }
        });
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String abc = Arrays.toString(selectedarray);
                abc = abc.replace("[","");
                abc = abc.replace("]","");
                abc = abc.replace(" ","");
                abc = abc.replace(",,",",");
                abc = abc.replace(",null","");
                abc = abc.replace("null,","");
                abc = abc.replace(",@","");
                abc = abc.replace("@,","");
                abc = abc.replace("@","");



                    editText.setText(abc);


            }
        });
        builder.show();
    }
    public static void multichoiceitem1(Context context, final String[] items, final EditText editText, final EditText editText1, String title) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select " + title);
        final String[] selectedarray = new String[items.length];
        builder.setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                if (isChecked){
                    selectedarray[which] = items[which];
                }else{
                    selectedarray[which] = "@";
                }
            }
        });
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String abc = Arrays.toString(selectedarray);
                abc = abc.replace("[","");
                abc = abc.replace("]","");
                abc = abc.replace(" ","");
                abc = abc.replace(",,",",");
                abc = abc.replace(",null","");
                abc = abc.replace("null,","");
                abc = abc.replace("null","");
                abc = abc.replace(",@","");
                abc = abc.replace("@,","");
                abc = abc.replace("@","");


                if (abc.contains("Hindu")||abc.contains("Jain")||abc.contains("Muslim")||abc.contains("Sikh")||abc.contains("Christian")){
                    editText1.setVisibility(View.VISIBLE);
                    editText.setText(abc);
                }
                else{
                    editText1.setVisibility(View.GONE);
                    editText.setText(abc);
                }

            }
        });
        builder.show();
    }

    public static void showspinner(Context context, final String[] items, final TextView textview) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                textview.setText(items[item]);
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void showspinnerriligion(Context context, final String[] items, final TextView textview, final TextView editText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
              String abc = items[item];
                if (abc.contains("Hindu")||abc.contains("Jain")||abc.contains("Muslim")||abc.contains("Sikh")||abc.contains("Christian")){
                    editText.setVisibility(View.VISIBLE);
                    editText.setText("");
                    textview.setText(abc);
                }
                else{
                    editText.setVisibility(View.GONE);
                    textview.setText(abc);
                }
                textview.setText(items[item]);
                dialog.dismiss();
            }
        });
        builder.show();
    }



    public static boolean isValidEmailId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }


    //check permission and allow by user in App
    public static boolean checkAndRequestPermissions(Context context) {
        int camerapermission = ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int readpermission = ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE);
        int internetpermission = ContextCompat.checkSelfPermission(context, Manifest.permission.INTERNET);
        int wifiaccespermisson = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_WIFI_STATE);
        int networkpermission = ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_NETWORK_STATE);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (readpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (internetpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.INTERNET);
        }
        if (wifiaccespermisson != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_WIFI_STATE);
        }
        if (networkpermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_NETWORK_STATE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions((Activity) context, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }
    public static boolean isValidMobile(String phone) {
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            return phone.length() > 8 && phone.length() <= 10;
        }
        return false;
    }
    private boolean isValidMobile1(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }
}
