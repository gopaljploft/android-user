package com.cab_taxi.infinitecabs.LoginSignUp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Background_Service.StatusManager_Background_Store_Data;
import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;
import com.cab_taxi.infinitecabs.Dashboard.Dashboard;
import com.cab_taxi.infinitecabs.DrawerActivities.Setting.EditProfile;
import com.cab_taxi.infinitecabs.Extra.Utility;
import com.cab_taxi.infinitecabs.Model.EmailVaildetionModel;
import com.cab_taxi.infinitecabs.Model.LoginModel;
import com.cab_taxi.infinitecabs.Model.User_Status_Background_Model;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;
import com.cab_taxi.infinitecabs.WaveView.WaveView;
import com.google.android.gms.location.LocationRequest;
import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity implements View.OnClickListener {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final String TAG = Login.class.getSimpleName();
    UserSessionManager session;
    LocationRequest mLocationRequest;
    ///Driver status check local data
    StatusManager_Background_Store_Data statusManager_background_store_data;
    HashMap<String, String> background_store_data;
    private WaveView waveView;
    private EditText et_email;
    private ImageView iv_clear;
    private EditText et_password;
    private ImageView iv_showPassword;
    private LinearLayout ll_password;
    private ImageView iv_back;
    public static final String MyPREFERENCES = "Firbase_Token";
    SharedPreferences sharedpreferences;
    public String Token;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.black_text_color));
        session = new UserSessionManager(getApplicationContext());
        statusManager_background_store_data = new StatusManager_Background_Store_Data(getApplicationContext());
        background_store_data = statusManager_background_store_data.getUserDetails();
        noInternetDialog = new NoInternetDialog.Builder(Login.this).build();

        initView();
    }
    NoInternetDialog noInternetDialog;
    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }
    private void initView() {
        TextView tv_signup = findViewById(R.id.tv_signup);
        ll_password = findViewById(R.id.ll_password);
        iv_back = findViewById(R.id.iv_back);
        et_email = findViewById(R.id.et_email);
        iv_clear = findViewById(R.id.iv_clear);
        et_password = findViewById(R.id.et_password);
        TextView tv_forgot_password = findViewById(R.id.tv_forgot_password);
        Button btn_login = findViewById(R.id.btn_login);

        tv_signup.setOnClickListener(this);
        iv_clear.setOnClickListener(this);
        tv_forgot_password.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        iv_back.setOnClickListener(this);

        et_email.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0)
                    iv_clear.setVisibility(View.VISIBLE);
                else {
                    iv_clear.setVisibility(View.GONE);
                }
            }
        });

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        Token = getSharedPreferences("Firbase_Token", MODE_PRIVATE).getString("regId", "");

        Map<String, String> map = new HashMap<>();
        map.put("user_id", "127");
        User_Status_Check_Background_Store(map);
        Log.d("not dead", "not dead");

        checkLocationPermission();
    }


    private void checkLocationPermission() {


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(Login.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_signup:
                startActivity(new Intent(Login.this, SignUp.class).putExtra("email", et_email.getText().toString().trim()));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;

            case R.id.iv_clear:
                et_email.setText("");
                break;
            case R.id.iv_back:
                ll_password.setVisibility(View.GONE);
                et_email.setEnabled(true);
                iv_clear.setVisibility(View.VISIBLE);
                iv_back.setVisibility(View.GONE);
                break;

            case R.id.tv_forgot_password:
                startActivity(new Intent(Login.this, ForgotPassword.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;

            case R.id.btn_login:
                submit();
//                startActivity(new Intent(Login.this, Dashboard.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;
        }
    }

    private void submit() {

        String email = et_email.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {

            CustomToast.makeText(getApplicationContext(), "Please enter Email", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

//            Toast.makeText(this, "Enter Email", Toast.LENGTH_SHORT).show();
            return;
        }
        if (ll_password.getVisibility() == View.VISIBLE) {
            String password = et_password.getText().toString().trim();
            if (TextUtils.isEmpty(password)) {
                CustomToast.makeText(getApplicationContext(), "Please enter Password", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

                return;
            }
            Map<String, String> map = new HashMap<>();
            map.put("email", email);
            map.put("password", password);
            map.put("deviceToken", Token);
            Login(map);
        } else {
            Map<String, String> map = new HashMap<>();
            map.put("email", email);
            emailExists(map);
        }

    }

    public void Login(final Map<String, String> map) {

        final Dialog dialog = new Dialog(Login.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<LoginModel> call = Apis.getAPIService().Loginapi(map);

        call.enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                LoginModel user = response.body();

                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                        if (String.valueOf(user.getData().getId()).equals(session.isLoggedIn())) {
                            CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
                           if (user.getData().getOtpstatus().equals("0")){
                               startActivity(new Intent(Login.this,Verification.class).putExtra("userId",String.valueOf(user.getData().getId())).putExtra("tag","1"));
                           }else{
                               Utility.setCard(Login.this,user.getData().getCardId(),user.getData().getPaymentMode());
                               session.setLogin();
                               startActivity(new Intent(Login.this, Dashboard.class));
                           }

                        } else {
                            session.iseditor();
                            session.createUserLoginSession(String.valueOf(user.getData().getId()),
                                    user.getData().getFirst_name(), user.getData().getLast_name(),
                                    user.getData().getEmail(), user.getData().getMobile(),
                                    user.getData().getUser_status(), user.getData().getImage(), user.getData().getNotification());
                            Utility.setdetail(Login.this,user.getData().getImage());
                            Utility.setCountryCode(Login.this,user.getData().getPhoneCode());
                            CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                            if (user.getData().getOtpstatus().equals("0")){
                                startActivity(new Intent(Login.this,Verification.class).putExtra("userId",String.valueOf(user.getData().getId())).putExtra("tag","1"));

                            }else{
                                Utility.setCard(Login.this,user.getData().getCardId(),user.getData().getPaymentMode());
                                session.setLogin();
                                startActivity(new Intent(Login.this, Dashboard.class));
                            }
                        }
                    } else {
                        CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.WARNING, true).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
//                            CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void emailExists(final Map<String, String> map) {
        final Dialog dialog = new Dialog(Login.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<EmailVaildetionModel> call = Apis.getAPIService().emailExists(map);
        call.enqueue(new Callback<EmailVaildetionModel>() {
            @Override
            public void onResponse(Call<EmailVaildetionModel> call, Response<EmailVaildetionModel> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                EmailVaildetionModel user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                        ll_password.setVisibility(View.VISIBLE);
                        et_email.setEnabled(false);
                        iv_clear.setVisibility(View.GONE);
                        iv_back.setVisibility(View.VISIBLE);
                    } else {
                        ll_password.setVisibility(View.GONE);
                        startActivity(new Intent(Login.this, SignUp.class).putExtra("email", et_email.getText().toString().trim()));
                    }
                } else {
                    CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
                }
            }

            @Override
            public void onFailure(Call<EmailVaildetionModel> call, Throwable t) {
//                            CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void showmessage(String message) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    // driver status in background API-------------------------
    private void User_Status_Check_Background_Store(Map<String, String> map) {
        Call<User_Status_Background_Model> call = Apis.getAPIService().getUserStatus(map);
        call.enqueue(new Callback<User_Status_Background_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<User_Status_Background_Model> call, Response<User_Status_Background_Model> response) {
                // dialog.dismiss();
                final User_Status_Background_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                        switch (user.getBookingData().getStatus()) {
                            case "Accepted":
                                statusManager_background_store_data.iseditor();
                                statusManager_background_store_data.createUserLoginSession(String.valueOf(user.getBookingData().getBookingId()),
                                        user.getBookingData().getPickupLocation(), user.getBookingData().getDropLocation(),
                                        user.getBookingData().getDriverName(), user.getBookingData().getDriverId(), user.getBookingData().getDriverMobile(),
                                        user.getBookingData().getDriverImage(), user.getBookingData().getBookingdate(), user.getBookingData().getPickuplat(),
                                        user.getBookingData().getPickuplng(), user.getBookingData().getDroplat(), user.getBookingData().getDroplng(), user.getBookingData().getStatus()
                                        , user.getBookingData().getDriverEmail(), user.getBookingData().getDriverlicense_no(), user.getBookingData().getDrivercar_type(), user.getBookingData().getDriverCar_Model(),
                                        user.getBookingData().getUserstarrating());
                                break;

                        }

                    } else {

                    }

                }
            }

            @Override
            public void onFailure(Call<User_Status_Background_Model> call, Throwable t) {
                //dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
}
