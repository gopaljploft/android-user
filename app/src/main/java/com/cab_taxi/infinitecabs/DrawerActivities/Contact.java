package com.cab_taxi.infinitecabs.DrawerActivities;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;
import com.cab_taxi.infinitecabs.Model.Contact_Us_Model;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Contact extends AppCompatActivity implements View.OnClickListener {
    private ImageView iv_back;
    private EditText et_message;
    private EditText rt_email;
    private Button btn_submit;
    UserSessionManager session;
    HashMap<String, String> user;
    private EditText rt_name;
    private EditText rt_mobile;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact);
        noInternetDialog = new NoInternetDialog.Builder(Contact.this).build();

        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.black_text_color));
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        initView();

    }

    NoInternetDialog noInternetDialog;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }

    private void initView() {
        iv_back = findViewById(R.id.iv_back);
        et_message = findViewById(R.id.et_message);
        rt_email = findViewById(R.id.rt_email);
        btn_submit = findViewById(R.id.btn_submit);
        iv_back.setOnClickListener(this);
        btn_submit.setOnClickListener(this);
        rt_name = (EditText) findViewById(R.id.rt_name);
        rt_name.setOnClickListener(this);
        rt_mobile = (EditText) findViewById(R.id.rt_mobile);
        rt_mobile.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                submit();
                break;

            case R.id.iv_back:
                onBackPressed();
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                break;
        }
    }

    private void submit() {
        // validate
        String message = et_message.getText().toString().trim();
        if (TextUtils.isEmpty(message)) {
            Toast.makeText(this, "Your Message", Toast.LENGTH_SHORT).show();
            return;
        }
        // validate
        String name = rt_name.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, "Your Name", Toast.LENGTH_SHORT).show();
            return;
        }

        String mobile = rt_mobile.getText().toString().trim();
        if (TextUtils.isEmpty(mobile)) {
            Toast.makeText(this, "Your Mobile number", Toast.LENGTH_SHORT).show();
            return;
        }
        String email = rt_email.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Your Email Address", Toast.LENGTH_SHORT).show();
            return;
        }

        Map<String, String> map = new HashMap<>();
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        map.put("message", message);
        map.put("name", name);
        map.put("mobile", mobile);
        map.put("email", email);
        Submit_Contact(map);


    }

    public void Submit_Contact(final Map<String, String> map) {

        final Dialog dialog = new Dialog(Contact.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Contact_Us_Model> call = Apis.getAPIService().submit_contact(map);

        call.enqueue(new Callback<Contact_Us_Model>() {
            @Override
            public void onResponse(Call<Contact_Us_Model> call, Response<Contact_Us_Model> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Contact_Us_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                        et_message.setText("");
                        rt_name.setText("");
                        rt_mobile.setText("");
                        rt_email.setText("");
                        CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
                    } else {
                        CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                    }
                }
            }
            @Override
            public void onFailure(Call<Contact_Us_Model> call, Throwable t) {
//        CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }


}
