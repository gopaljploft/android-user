package com.cab_taxi.infinitecabs.DrawerActivities.My_Trips;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;
import com.cab_taxi.infinitecabs.Dashboard.Dashboard;
import com.cab_taxi.infinitecabs.Model.Cancel_Ride_Model;
import com.cab_taxi.infinitecabs.Model.UserUpcommingRideModel;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpcomingFragment extends Fragment implements View.OnClickListener {
    UserSessionManager session;
    HashMap<String, String> user;
    UpcoingAdapter upcoingAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    String pickuplat, picklong, droplat, droplong;
    private RecyclerView recyclerCompeletd;
    RelativeLayout rlnodata;
    private List<UserUpcommingRideModel.Data> array_upconing_ride_list;
    private Button btn_getStart;

    public UpcomingFragment() {
        // Required empty public constructor
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upcoming, container, false);
        Window window = getActivity().getWindow();
        // clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        // finally change the color
        window.setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.black_text_color));

        session = new UserSessionManager(getActivity());
        user = session.getUserDetails();
        initView(view);
        return view;
    }

    private void initView(View view) {
        Log.d("asdfh","1");
        btn_getStart = view.findViewById(R.id.btn_getStart);
        rlnodata = view.findViewById(R.id.rlnodata);
        btn_getStart.setOnClickListener(this);
        recyclerCompeletd = view.findViewById(R.id.recyclerupcoing);
        swipeRefreshLayout = view.findViewById(R.id.swipeToRefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorRed);
        swipeRefreshLayout.setSize(23);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Map<String, String> map = new HashMap<>();
                map.put("user_id", user.get(UserSessionManager.KEY_ID));
                upcoingRide(map);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        Map<String, String> map = new HashMap<>();
        map.put("user_id",user.get(UserSessionManager.KEY_ID));
        upcoingRide(map);
    }

    public void onClick(View v) {
        if (v.getId() == R.id.btn_getStart) {
            startActivity(new Intent(getActivity(), Dashboard.class));
        }

    }

    private void upcoingRide(Map<String, String> map) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<UserUpcommingRideModel> call = Apis.getAPIService().userUpcommingRide(map);
        call.enqueue(new Callback<UserUpcommingRideModel>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<UserUpcommingRideModel> call, Response<UserUpcommingRideModel> response) {
                dialog.dismiss();
                UserUpcommingRideModel userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {
                        if (userdata.getData().size()>0) {
                            rlnodata.setVisibility(View.GONE);
                            swipeRefreshLayout.setVisibility(View.VISIBLE);
                            array_upconing_ride_list = new ArrayList<>();
                            array_upconing_ride_list.addAll(userdata.getData());
                            upcoingAdapter = new UpcoingAdapter();
                            recyclerCompeletd.setAdapter(upcoingAdapter);
                            upcoingAdapter.notifyDataSetChanged();
                        } else {
                            rlnodata.setVisibility(View.VISIBLE);
//                            no_data_found.setText(userdata.getMessage());
                            swipeRefreshLayout.setVisibility(View.GONE);
                        }
                    } else {
                        rlnodata.setVisibility(View.VISIBLE);
//                        rlnodata.setText(userdata.getMessage());
                        swipeRefreshLayout.setVisibility(View.GONE);
                        //   CustomToast.makeText(getActivity(), userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<UserUpcommingRideModel> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }
    //    <-----------------------------booking cancel API  --------------------------------------------------------->
    public void Cancel_Booking(final Map<String, String> map) {
    /*  Dialog dialog = CustomDialog.showProgressDialog(LoginActivity.this);
        dialog.show(); */
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Cancel_Ride_Model> call = Apis.getAPIService().cancelRide(map);
        call.enqueue(new Callback<Cancel_Ride_Model>() {
            @Override
            public void onResponse(Call<Cancel_Ride_Model> call, Response<Cancel_Ride_Model> response) {
                dialog.dismiss();
                Cancel_Ride_Model cancel_ride_model = response.body();
                if (cancel_ride_model != null) {
                    if (cancel_ride_model.getStatusCode().equals("200")) {
//                        cancelRideFromRequestAcceptedLayout();
//                        startActivity(new Intent(getActivity(), Dashboard.class));
                        Map<String, String> map = new HashMap<>();
                        //(email,password)
                        map.put("user_id", user.get(UserSessionManager.KEY_ID));
                        upcoingRide(map);
                        CustomToast.makeText(getActivity(), cancel_ride_model.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
                    } else {
                        CustomToast.makeText(getActivity(), cancel_ride_model.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                    }
                } else {
                    CustomToast.makeText(getActivity(), "Something wents Worng", CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                }
            }

            @Override
            public void onFailure(Call<Cancel_Ride_Model> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public class UpcoingAdapter extends RecyclerView.Adapter<UpcoingAdapter.MyViewHolder> {


        public UpcoingAdapter() {
        }

        @Override
        public UpcoingAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(getActivity()).inflate(R.layout.upcoming_cards, parent, false);
            return new UpcoingAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final UpcoingAdapter.MyViewHolder holder, final int i) {
//            holder.tv_serviceType.setText(array_upconing_ride_list.get(i).getDriver_name());
            holder.tv_priceToday.setText("$" + "N/A");
            holder.cardbottom.setVisibility(View.GONE);
            String oldFormat= "dd/MM/yyyy HH:mm";
            String newFormat= "dd/MM/yyyy";
            String newFormat2= "HH:mm";
            String mStringDate = "25-11-15 14:23:34";
            String formatedDate = "";
            String formatedDate2 = "";
            SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat);
            Date myDate = null;
            Log.d("Asdfasdf",array_upconing_ride_list.get(i).getBook_create_date_time());
            try {
                myDate = dateFormat.parse(array_upconing_ride_list.get(i).getBook_create_date_time()+":00");
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            SimpleDateFormat timeFormat = new SimpleDateFormat(newFormat);
            formatedDate = timeFormat.format(myDate);


             SimpleDateFormat dateFormat2 = new SimpleDateFormat(oldFormat);
            Date myDate2 = null;
            try {
                myDate2 = dateFormat2.parse(array_upconing_ride_list.get(i).getBook_create_date_time());
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            SimpleDateFormat timeFormat2 = new SimpleDateFormat(newFormat2);
            formatedDate2 = timeFormat2.format(myDate2);
            if (array_upconing_ride_list.get(i).getBook_scheduled_time().isEmpty()){
                holder.picktime.setText("Pick Date & Time: N/A");
            }else{
                getvide(holder.picktime,array_upconing_ride_list.get(i).getBook_scheduled_time());
            }

//           ;
            holder.tv_rideTime.setText("Date: "+formatedDate);
            holder.tv_serviceType.setText("Booking ID: "+array_upconing_ride_list.get(i).getBooking_id());
            holder.tv_rideTime_.setText("Time: "+formatedDate2);
           //            holder.tv_date_time.setText("Date/Time: "+array_upconing_ride_list.get(i).getBook_create_date_time());
            holder.tv_previousRideCharge.setText("Total Fare: $"+"N/A");
            if (array_upconing_ride_list.get(i).getCar_type().isEmpty()){
                holder.tv_carCategory.setText("Cab Type: N/A");
            }else{
                holder.tv_carCategory.setText("Cab Type: "+array_upconing_ride_list.get(i).getCar_type());
            }
            holder.tv_to.setText(""+array_upconing_ride_list.get(i).getDrop_area());
            holder.tv_from.setText(array_upconing_ride_list.get(i).getPickup_area());
//            holder.tv_rideTime.setText(array_upconing_ride_list.get(i).getDriver_car_number());
       /*     pickuplat = array_upconing_ride_list.get(i).getUser_drop_latitude();
            picklong = array_upconing_ride_list.get(i).getUser_drop_long_latitude();
            droplat = array_upconing_ride_list.get(i).getUser_pickup_latitude();
            droplong = array_upconing_ride_list.get(i).getUser_pickup_long_latitude();*/
          /*  holder.mMapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    MapsInitializer.initialize(getActivity());
                }
            });*/
          holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                  sweetAlertDialog.setTitleText("Infinite Cabs");
                  sweetAlertDialog.setContentText("Are you sure you want to cancel your ride?");
                  sweetAlertDialog.setConfirmText("Yes");
                  sweetAlertDialog.setCancelText("No");
                  sweetAlertDialog.setCustomImage(getResources().getDrawable(R.drawable.girl));
                  sweetAlertDialog.showCancelButton(false);
                  sweetAlertDialog.setCancelable(true);
                  sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                      @Override
                      public void onClick(SweetAlertDialog sDialog) {
                          sDialog.cancel();
                          Map<String, String> map = new HashMap<>();
                          map.put("user_id", user.get(UserSessionManager.KEY_ID));
                          map.put("reason_id", "0");
                          map.put("driver_id", "0");
                          map.put("type", "user");
                          map.put("note", "0");
                          map.put("booking_id", array_upconing_ride_list.get(i).getBooking_id()+"");
                          Cancel_Booking(map);
                      }
                  });
                  sweetAlertDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                      @Override
                      public void onClick(SweetAlertDialog sDialog) {
                          sDialog.cancel();
                      }
                  });
                  sweetAlertDialog.show();



              }
          });
            holder.carduppor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (holder.cardbottom.getVisibility() == View.VISIBLE) {
                        holder.cardbottom.setVisibility(View.GONE);
                    } else {
                        holder.cardbottom.setVisibility(View.VISIBLE);
                    }

                }
            });

        }

        @Override
        public int getItemCount() {
            return array_upconing_ride_list.size();
        }


        public long getItemId(int position) {
            return position;
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {

            private CardView carduppor;
            private CardView cardbottom;
            private TextView tv_serviceType;
            private TextView tv_priceToday;
            private TextView tv_rideTime;
            private TextView tvlocation;
            private TextView tv_previousRideCharge;
            private TextView tv_carCategory;
            private TextView tv_from;
            private TextView tv_to,tv_rideTime_;
            private TextView tv_date_time;
            private TextView picktime;
            private Button ivdecline;
            private Button ivaccept;
            private Button btn_cancel;
//            private MapView mMapView;

            public MyViewHolder(View v) {
                super(v);
                carduppor = v.findViewById(R.id.carduppor);
                tv_serviceType = v.findViewById(R.id.tv_serviceType);
                tv_priceToday = v.findViewById(R.id.tv_priceToday);
                tv_date_time = v.findViewById(R.id.tv_date_time);
                picktime = v.findViewById(R.id.picktime);
//                mMapView = v.findViewById(R.id.mapView);
                tv_rideTime = v.findViewById(R.id.tv_rideTime);
                tv_rideTime_ = v.findViewById(R.id.tv_rideTime_);
                tv_to = v.findViewById(R.id.tv_to);
                tv_from = v.findViewById(R.id.tv_from);
                tv_carCategory = v.findViewById(R.id.tv_carCategory);
                tv_previousRideCharge = v.findViewById(R.id.tv_previousRideCharge);
                cardbottom = v.findViewById(R.id.cardbottom);
                btn_cancel = v.findViewById(R.id.btn_cancel);
            }
        }
    }
    public void getvide(TextView textView,String string){
        String oldFormat= "dd/MM/yyyy HH:mm";
        String newFormat= "dd/MM/yyyy,HH:mm";
        String formatedDate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat(oldFormat);
        Date myDate = null;
        try {
            myDate = dateFormat.parse(string);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat timeFormat = new SimpleDateFormat(newFormat);
        formatedDate = timeFormat.format(myDate);
        textView.setText("Pick Date & Time: "+formatedDate);

    }
    @Override
    public void onResume() {
        super.onResume();
        Map<String, String> map = new HashMap<>();
        //(email,password)
        //5
        map.put("user_id",user.get(UserSessionManager.KEY_ID));
        upcoingRide(map);
    }
}
