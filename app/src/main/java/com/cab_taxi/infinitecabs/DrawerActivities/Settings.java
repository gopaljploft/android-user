package com.cab_taxi.infinitecabs.DrawerActivities;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Background_Service.StatusManager_Background_Store_Data;
import com.cab_taxi.infinitecabs.DrawerActivities.Setting.AboutUs;
import com.cab_taxi.infinitecabs.DrawerActivities.Setting.EditProfile;
import com.cab_taxi.infinitecabs.DrawerActivities.Setting.PrivacyPolicy;
import com.cab_taxi.infinitecabs.DrawerActivities.Setting.Terms_and_conditions;
import com.cab_taxi.infinitecabs.Model.Notification_ON_OFF_Model;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;
import com.github.angads25.toggle.interfaces.OnToggledListener;
import com.github.angads25.toggle.model.ToggleableView;
import com.github.angads25.toggle.widget.LabeledSwitch;
import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Settings extends AppCompatActivity implements View.OnClickListener {


    UserSessionManager session;
    StatusManager_Background_Store_Data statusManager_background_store_data;
    HashMap<String, String> user;
    String notification_status;
    private ImageView iv_close;
    private TextView tv_editProfile;
    private TextView tv_changePassword;
    private TextView tv_staticPages;
    private TextView tv_notification;
    private LabeledSwitch switchCustom;
    private TextView tv_appVersion;
    private TextView tv_logOut;
    private TextView tv_aboutUs;
    private TextView tv_version;
    private TextView tv_privacy;
    private TextView tv_terms;
    private RelativeLayout nav_legal;
    private AppCompatTextView tvlegal;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        statusManager_background_store_data = new StatusManager_Background_Store_Data(getApplicationContext());
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.black_text_color));
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();

        initView();
    }

    private void initView() {

        iv_close = findViewById(R.id.iv_close);
        tv_editProfile = findViewById(R.id.tv_editProfile);
        tv_staticPages = findViewById(R.id.tv_staticPages);
/*
        tv_notification = findViewById(R.id.tv_notification);
*/
        switchCustom = findViewById(R.id.switchCustom);
        tv_appVersion = findViewById(R.id.tv_appVersion);
        tv_logOut = findViewById(R.id.tv_logOut);
        tv_aboutUs = findViewById(R.id.tv_aboutUs);

        iv_close.setOnClickListener(this);
        tv_editProfile.setOnClickListener(this);
        tv_staticPages.setOnClickListener(this);
/*
        tv_notification.setOnClickListener(this);
*/
        notification_status = user.get(UserSessionManager.KEY_NOTIFICATION);
        if (notification_status.equals("1")) {
            switchCustom.setOn(true);
        } else {
            switchCustom.setOn(false);
        }

        switchCustom.setOnToggledListener(new OnToggledListener() {
            @Override
            public void onSwitched(ToggleableView toggleableView, boolean isOn) {
                // Implement your switching logic here
                String notification_status = user.get(UserSessionManager.KEY_NOTIFICATION);
                if (notification_status.equals("1")) {
                    Map<String, String> map = new HashMap<>();
                    map.put("userId", user.get(UserSessionManager.KEY_ID));
                    map.put("notification", "2");
                    Notification(map);
                } else {
                    Map<String, String> map = new HashMap<>();
                    map.put("userId", user.get(UserSessionManager.KEY_ID));
                    map.put("notification", "1");
                    Notification(map);

                }

            }
        });
        //tv_appVersion.setOnClickListener(this);
        tv_logOut.setOnClickListener(this);
        tv_aboutUs.setOnClickListener(this);

        tv_version = (TextView) findViewById(R.id.tv_version);
        tv_version.setOnClickListener(this);
        tv_privacy = (TextView) findViewById(R.id.tv_privacy);
        tv_privacy.setOnClickListener(this);
        tv_terms = (TextView) findViewById(R.id.tv_terms);
        tv_terms.setOnClickListener(this);
        nav_legal = (RelativeLayout) findViewById(R.id.nav_legal);
        nav_legal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Settings.this, Legal.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }
        });
        tvlegal = (AppCompatTextView) findViewById(R.id.tvlegal);
        tvlegal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Settings.this, Legal.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_close:
                onBackPressed();
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

                break;

            case R.id.tv_editProfile:
                startActivity(new Intent(this, EditProfile.class).putExtra("tag", "2"));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;

            case R.id.tv_staticPages:

                Toast.makeText(this, "You clicked on Static pages", Toast.LENGTH_SHORT).show();
                break;

            case R.id.tv_appVersion:
                //Toast.makeText(this, "App version 6.5", Toast.LENGTH_LONG ).show();
                break;

            case R.id.tv_logOut:
                session.logoutUser();
                statusManager_background_store_data.iseditor();
                //  startActivity(new Intent(this, Login.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                break;

            case R.id.tv_aboutUs:
                startActivity(new Intent(this, AboutUs.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;
            case R.id.tv_terms:
                startActivity(new Intent(this, Terms_and_conditions.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;
            case R.id.tv_privacy:
                startActivity(new Intent(this, PrivacyPolicy.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;

        }
    }

    public void Notification(final Map<String, String> map) {
    /*   Dialog dialog = CustomDialog.showProgressDialog(LoginActivity.this);
        dialog.show();  */
        final Dialog dialog = new Dialog(Settings.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Notification_ON_OFF_Model> call = Apis.getAPIService().notification(map);

        call.enqueue(new Callback<Notification_ON_OFF_Model>() {
            @Override
            public void onResponse(Call<Notification_ON_OFF_Model> call, Response<Notification_ON_OFF_Model> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Notification_ON_OFF_Model user1 = response.body();
                if (user1 != null) {
                    if (user1.getStatusCode().equals("200")) {
                        session.iseditor();
                        session.createUserLoginSession(String.valueOf(user.get(UserSessionManager.KEY_ID)),
                                user.get(UserSessionManager.KEY_NAME), user.get(UserSessionManager.KEY_NAME_LAST),
                                user.get(UserSessionManager.KEY_EMAIL), user.get(UserSessionManager.KEY_MOBILE), "", user.get(UserSessionManager.KEY_IMAGE), user1.getData().getNotification());
                        //  showmessage(user1.getMessage());
                    } else {
                        showmessage(user1.getMessage());
                    }

                } else {

                    showmessage("Something wents Worng");
                }
            }

            @Override
            public void onFailure(Call<Notification_ON_OFF_Model> call, Throwable t) {
//        CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }

    public void showmessage(String message) {
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                })
                .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
                .show();
    }

}
