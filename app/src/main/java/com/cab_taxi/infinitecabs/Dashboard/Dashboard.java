package com.cab_taxi.infinitecabs.Dashboard;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.DrawerActivities.Contact;
import com.cab_taxi.infinitecabs.DrawerActivities.HomeFragment;
import com.cab_taxi.infinitecabs.DrawerActivities.Legal;
import com.cab_taxi.infinitecabs.DrawerActivities.MyTrips;
import com.cab_taxi.infinitecabs.DrawerActivities.OfferScreen;
import com.cab_taxi.infinitecabs.DrawerActivities.Setting.EditProfile;
import com.cab_taxi.infinitecabs.DrawerActivities.Settings;
import com.cab_taxi.infinitecabs.Extra.Utility;
import com.cab_taxi.infinitecabs.LocationUtil.PermissionUtils;
import com.cab_taxi.infinitecabs.Model.User_Lat_Lng_Send_Model;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.Payment.PaymentMode;
import com.cab_taxi.infinitecabs.R;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;
import com.cab_taxi.infinitecabs.Track_Location.LocationTrack;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.navigation.NavigationView;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dashboard extends AppCompatActivity implements ActivityCompat.OnRequestPermissionsResultCallback,
        PermissionUtils.PermissionResultCallback, View.OnClickListener {
    public static final String MY_PREFS_NAME = "MyPrefsFile";
    private final static int PLAY_SERVICES_REQUEST = 1000;
    private final static int REQUEST_CHECK_SETTINGS = 2000;
    private static final int DRAW_OVER_OTHER_APP_PERMISSION = 123;
    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 20000;
    Double latitude_driver;
    Double longitude_driver;
    NoInternetDialog noInternetDialog;
    SharedPreferences editor;
    double latitude;
    double longitude;
    ArrayList<String> permissions = new ArrayList<>();
    PermissionUtils permissionUtils;
    boolean isPermissionGranted;
    UserSessionManager session;
    HashMap<String, String> user;
    LocationTrack locationTrack;
    private FrameLayout frame;
    private NavigationView nav_view;
    // Google client to interact with Google API
    private DrawerLayout drawer;
    private WebView webView;
    private TextView navigateionText;
    // list of permissions
    private double latitude_user;
    private double longitude_user;
    private ImageView forward_arrow_nav_header;
    private LinearLayout layout_profile;
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private TextView tv_name_profile;
    private CircleImageView civProfileimage;
    private ImageView iv_home;
    private TextView tv_mobile;
    private LinearLayout call_number, whatsapp_call;
    private RelativeLayout nav_home, nav_my_trips, nav_my_cards, nav_settings, nav_legal, nav_contact, nav_offer;
    // location last updated time
    private String mLastUpdateTime;
    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    // boolean flag to toggle the ui
    private Boolean mRequestingLocationUpdates;
    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("receiver", "Got message");
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.black_text_color));
        noInternetDialog = new NoInternetDialog.Builder(Dashboard.this).build();
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        Log.d("klsdfl", ";asdjf");
        permissionUtils = new PermissionUtils(Dashboard.this);
        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionUtils.check_permission(permissions, "Need GPS permission for getting your location", 1);
//        askForSystemOverlayPermission();
        initView();
        restoreValuesFromBundle(savedInstanceState);
        init();
        setStatusBarTranslucent(true);
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {

        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    public void openDrawer() {
     /*   Log.d("asdfadsf", user.get(UserSessionManager.KEY_IMAGE));*/
       /* Picasso.get().load("http://admin.infinitecabs.com.au/user_image/" + Utility.getProfilePic(Dashboard.this))
                .error(R.drawable.user)
                .priority(Picasso.Priority.HIGH)
                .networkPolicy(NetworkPolicy.NO_CACHE).into(civProfileimage);*/
        try{
            RequestOptions options = new RequestOptions()
                    .placeholder(R.mipmap.ic_launcher_round)
                    .error(R.mipmap.ic_launcher_round);


            Glide.with(this).load("https://admin.infinitecabs.com.au/user_image/" + Utility.getProfilePic(Dashboard.this)).apply(options).into(civProfileimage);
        }catch (Exception e){
            e.printStackTrace();
        }

        drawer.openDrawer(GravityCompat.START);
    }

    public void addFragment(Fragment fragment) {
        HomeFragment homeFragment = new HomeFragment();
        FragmentManager fm1 = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm1.beginTransaction();
        fragmentTransaction.replace(R.id.frame, homeFragment);
        fragmentTransaction.commit();
    }

    private void init() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(Dashboard.this);
        mSettingsClient = LocationServices.getSettingsClient(Dashboard.this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                updateLocationUI();
            }
        };

        mRequestingLocationUpdates = false;
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        Log.d("vgdfgdf",result+"");
        return result;
    }
    public static void setMargins (View v, int l, int t, int r, int b) {
        if (v.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            p.setMargins(l, t, r, b);
            v.requestLayout();
        }
    }
    private void initView() {
        //  locationTrack = new LocationTrack(Dashboard.this);
        drawer = findViewById(R.id.drawer_layout);
        addFragment(new HomeFragment());
        nav_home = findViewById(R.id.nav_home);
        nav_my_trips = findViewById(R.id.nav_my_trips);
        nav_my_cards = findViewById(R.id.nav_my_cards);
        nav_settings = findViewById(R.id.nav_settings);
        nav_legal = findViewById(R.id.nav_legal);
        nav_contact = findViewById(R.id.nav_contact);
        nav_offer = findViewById(R.id.nav_offer);

        call_number = findViewById(R.id.call_number);
        whatsapp_call = findViewById(R.id.whatsapp_call);
        frame = findViewById(R.id.frame);

        nav_view = findViewById(R.id.nav_view);
//        setMargins(nav_view,0,getStatusBarHeight(),0,0);
        nav_view.setPadding(0,getStatusBarHeight(),0,0);
        drawer = findViewById(R.id.drawer_layout);

        //  View headerView = nav_view.getHeaderView(0);
        layout_profile = findViewById(R.id.header);
        tv_name_profile = findViewById(R.id.tv_name_profile);
        civProfileimage = findViewById(R.id.user_image_nav);
        tv_mobile = findViewById(R.id.tv_mobile);

        tv_name_profile.setText(user.get(UserSessionManager.KEY_NAME));
        tv_mobile.setText(Utility.getCountryCode(Dashboard.this)+""+user.get(UserSessionManager.KEY_MOBILE));
       /* Picasso.get().load("http://admin.infinitecabs.com.au/user_image/" + user.get(UserSessionManager.KEY_IMAGE))
                .error(R.drawable.user)
                .priority(Picasso.Priority.HIGH)
                .networkPolicy(NetworkPolicy.NO_CACHE).into(civProfileimage);*/
        layout_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Dashboard.this, EditProfile.class).putExtra("tag", "1"));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }
        });

        call_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:+611300103100"));
                startActivity(callIntent);
            }
        });
        whatsapp_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickWhatsApp();
            }
        });

        nav_home.setOnClickListener(this);
        nav_my_trips.setOnClickListener(this);
        nav_my_cards.setOnClickListener(this);
        nav_settings.setOnClickListener(this);
        nav_legal.setOnClickListener(this);
        nav_contact.setOnClickListener(this);
        nav_offer.setOnClickListener(this);

    }

    public void onClickWhatsApp() {


        PackageManager pm = Dashboard.this.getPackageManager();
        try {

            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            String text = "YOUR TEXT HERE";
            PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
//            startActivity(Intent.createChooser(waIntent, "Share with"));
            Intent sendIntent = new Intent("android.intent.action.MAIN");
            sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators("61 469 889 047") + "@s.whatsapp.net");//phone number without "+" prefix

            startActivity(sendIntent);

//            String toNumber = "+91 98765 43210"; // contains spaces.
           /* String toNumber = "+61 469 889 047"; // contains spaces.
            toNumber = toNumber.replace("+", "").replace(" ", "");

            Intent sendIntent1 = new Intent("android.intent.action.MAIN");
            sendIntent.putExtra("jid", toNumber + "@s.whatsapp.net");
            sendIntent.putExtra(Intent.EXTRA_TEXT, "");
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.setPackage("com.whatsapp");
            sendIntent.setType("text/plain");
            startActivity(sendIntent1);*/
        } catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(Dashboard.this, "WhatsApp not Installed", Toast.LENGTH_SHORT)
                    .show();
        }
    }

    private void getLocation() {
/*
        if (isPermissionGranted) {
            try {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }*/

    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void PermissionGranted(int request_code) {
        Log.i("PERMISSION", "GRANTED");
        isPermissionGranted = true;
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
        Log.i("PERMISSION PARTIALLY", "GRANTED");
    }

    @Override
    public void PermissionDenied(int request_code) {
        Log.i("PERMISSION", "DENIED");
    }

    @Override
    public void NeverAskAgain(int request_code) {
        Log.i("PERMISSION", "NEVER ASK AGAIN");
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.nav_home) {
//            HomeFragment homeFragment = new HomeFragment();
//            FragmentManager fm1 = getSupportFragmentManager();
//            FragmentTransaction fragmentTransaction = fm1.beginTransaction();
//            fragmentTransaction.replace(R.id.frame, homeFragment);
//            fragmentTransaction.commit();

        } else if (id == R.id.iv_home) {
            HomeFragment homeFragment = new HomeFragment();
            FragmentManager fm1 = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm1.beginTransaction();
            fragmentTransaction.replace(R.id.frame, homeFragment);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_my_trips) {

            startActivity(new Intent(this, MyTrips.class));
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);

        } else if (id == R.id.nav_my_cards) {

            //   startActivity(new Intent(this, MyCards.class));
            startActivity(new Intent(this, PaymentMode.class));
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        } else if (id == R.id.nav_settings) {

            startActivity(new Intent(this, Settings.class));
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        } else if (id == R.id.nav_offer) {

            startActivity(new Intent(this, OfferScreen.class));
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        } else if (id == R.id.nav_legal) {

            startActivity(new Intent(this, Legal.class));
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        } else if (id == R.id.nav_contact) {

            startActivity(new Intent(this, Contact.class));
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private void Send_User_Lat_Lng(Map<String, String> map) {
        Call<User_Lat_Lng_Send_Model> call = Apis.getAPIService().SendLatLng(map);
        call.enqueue(new Callback<User_Lat_Lng_Send_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<User_Lat_Lng_Send_Model> call, Response<User_Lat_Lng_Send_Model> response) {
                // dialog.dismiss();
                final User_Lat_Lng_Send_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {

                    } else {
//                        CustomToast.makeText(Dashboard.this, user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<User_Lat_Lng_Send_Model> call, Throwable t) {
                //dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void errorToast() {
        Toast.makeText(this, "Draw over other app permission not available. Can't start the application without the permission.", Toast.LENGTH_LONG).show();
    }

    private void restoreValuesFromBundle(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("is_requesting_updates")) {
                mRequestingLocationUpdates = savedInstanceState.getBoolean("is_requesting_updates");
            }

            if (savedInstanceState.containsKey("last_known_location")) {
                mCurrentLocation = savedInstanceState.getParcelable("last_known_location");
            }

            if (savedInstanceState.containsKey("last_updated_on")) {
                mLastUpdateTime = savedInstanceState.getString("last_updated_on");
            }
        }

        updateLocationUI();
    }

    private void updateLocationUI() {
        if (mCurrentLocation != null) {
            latitude_driver = mCurrentLocation.getLatitude();
            longitude_driver = mCurrentLocation.getLongitude();
            try {
                Utility.latlng(Dashboard.this, latitude_driver, longitude_driver);
            } catch (Exception e) {
            }
            Map<String, String> map1 = new HashMap<>();
            map1.put("user_id", user.get(UserSessionManager.KEY_ID));
            map1.put("lat", latitude_driver + "");
            map1.put("lng", longitude_driver + "");
            Send_User_Lat_Lng(map1);
            Log.d("MapsActivity", "vikash88888 " + latitude_driver + " " + longitude_driver);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("is_requesting_updates", mRequestingLocationUpdates);
        outState.putParcelable("last_known_location", mCurrentLocation);
        outState.putString("last_updated_on", mLastUpdateTime);
    }

    private void startLocationUpdates() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(Dashboard.this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i("gfhgf", "All location settings are satisfied.");

//                        Toast.makeText(Dashboard.this, "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                        updateLocationUI();
                    }
                })
                .addOnFailureListener(Dashboard.this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i("iiiiiresult", "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(Dashboard.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i("iiiiiresult", "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e("iiiiiresult", errorMessage);

//                                Toast.makeText(Dashboard.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                        updateLocationUI();
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.e("iiiiiresult", "User agreed to make required location settings changes.");
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.e("iiiiiresult", "User chose not to make required location settings changes.");
                        mRequestingLocationUpdates = false;
                        break;
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            // LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(FILTER_STRING));

            iv_home.setVisibility(View.VISIBLE);
            startLocationUpdates();
            updateLocationUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }

}
