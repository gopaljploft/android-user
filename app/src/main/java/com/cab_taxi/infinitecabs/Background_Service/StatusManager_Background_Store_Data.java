package com.cab_taxi.infinitecabs.Background_Service;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;


public class StatusManager_Background_Store_Data {

    // Shared Preferences reference
    static SharedPreferences pref;

    // Editor reference for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREFER_NAME = "User Status Data";

    // User name (make variable public to access from outside)
    public static final String KEY_BOOKING_ID = "bookingId";
    public static final String KEY_PICKUP_LOCATION = "pickupLocation";
    public static final String KEY_DROP_LOCATION = "dropLocation";
    public static final String KEY_DTRIVERFIRSTNAME = "drivername";
    public static final String KEY_DRIVER_ID = "driverid";
    public static final String KEY_DRIVER_MOBILE = "drivermobile";
    public static final String KEY_DRIVER_IMAGE = "driverimage";
    public static final String KEY_BOOKING_DATE= "bookingdate";

    public static final String KEY_PICKUP_LAT = "pickuplat";
    public static final String KEY_PICKUP_LNG = "pickuplng";
    public static final String KEY_DROP_LAT= "droplat";
    public static final String KEY_DROP_LNG = "droplng";
    public static final String KEY_STATUS = "status";

    public static final String KEY_DRIVER_EMAIL = "driveremail";
    public static final String KEY_DRIVER_LICENCE = "driverlicence";
    public static final String KEY_DRIVER_CAR_TYPE = "drivercartype";
    public static final String KEY_DRIVER_CAR_MODEL = "drivercarmodel";
    public static final String KEY_DRIVER_RATING = "driverrating";



    // Constructor
    public StatusManager_Background_Store_Data(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    //Create login session
    public void createUserLoginSession(String bookingId, String pickupLocation, String dropLocation, String driverName, String driverid,
                                       String drivermobile,String driverimage,String bookingdate,String pickuplat,String pickuplng,String droplat,
                                        String droplng,String status ,String driveremail,String driverlicence,String drivercartype,String carmodel,String driverrating) {
        // Storing name in pref
        editor.putString(KEY_BOOKING_ID, bookingId);
        editor.putString(KEY_PICKUP_LOCATION, pickupLocation);
        editor.putString(KEY_DROP_LOCATION, dropLocation);
        editor.putString(KEY_DTRIVERFIRSTNAME, driverName);
        editor.putString(KEY_DRIVER_ID, driverid);
        editor.putString(KEY_DRIVER_MOBILE, drivermobile);
        editor.putString(KEY_DRIVER_IMAGE, driverimage);

        editor.putString(KEY_BOOKING_DATE, bookingdate);
        editor.putString(KEY_PICKUP_LAT, pickuplat);
        editor.putString(KEY_PICKUP_LNG, pickuplng);
        editor.putString(KEY_DROP_LAT, droplat);
        editor.putString(KEY_DROP_LNG, droplng);
        editor.putString(KEY_STATUS,status );
        editor.putString(KEY_DRIVER_EMAIL,driveremail );
        editor.putString(KEY_DRIVER_LICENCE,driverlicence );
        editor.putString(KEY_DRIVER_CAR_TYPE,drivercartype );
        editor.putString(KEY_DRIVER_CAR_MODEL,carmodel );
        editor.putString(KEY_DRIVER_RATING,driverrating );



        editor.apply();
        editor.commit();

    }



    /**
     * Get stored session data
     */
    public HashMap<String, String> getUserDetails() {
        //Use hashmap to store user credentials
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(KEY_BOOKING_ID, pref.getString(KEY_BOOKING_ID, null));
        user.put(KEY_PICKUP_LOCATION, pref.getString(KEY_PICKUP_LOCATION, null));
        user.put(KEY_DROP_LOCATION, pref.getString(KEY_DROP_LOCATION, null));
        user.put(KEY_DTRIVERFIRSTNAME, pref.getString(KEY_DTRIVERFIRSTNAME, null));
        user.put(KEY_DRIVER_ID, pref.getString(KEY_DRIVER_ID, null));
        user.put(KEY_DRIVER_MOBILE, pref.getString(KEY_DRIVER_MOBILE, null));
        user.put(KEY_DRIVER_IMAGE, pref.getString(KEY_DRIVER_IMAGE, null));
        user.put(KEY_BOOKING_DATE, pref.getString(KEY_BOOKING_DATE, null));
        user.put(KEY_PICKUP_LAT, pref.getString(KEY_PICKUP_LAT, null));
        user.put(KEY_PICKUP_LNG, pref.getString(KEY_PICKUP_LNG, null));
        user.put(KEY_DROP_LAT, pref.getString(KEY_DROP_LAT, null));
        user.put(KEY_DROP_LNG, pref.getString(KEY_DROP_LNG, null));
        user.put(KEY_STATUS, pref.getString(KEY_STATUS, null));


        user.put(KEY_DRIVER_EMAIL, pref.getString(KEY_DRIVER_EMAIL, null));
        user.put(KEY_DRIVER_LICENCE, pref.getString(KEY_DRIVER_LICENCE, null));
        user.put(KEY_DRIVER_CAR_TYPE, pref.getString(KEY_DRIVER_CAR_TYPE, null));
        user.put(KEY_DRIVER_CAR_MODEL, pref.getString(KEY_DRIVER_CAR_MODEL, null));
        user.put(KEY_DRIVER_RATING, pref.getString(KEY_DRIVER_RATING, null));


        // return user
        return user;
    }




    public void iseditor() {
        editor.clear();
        editor.commit();

    }
}