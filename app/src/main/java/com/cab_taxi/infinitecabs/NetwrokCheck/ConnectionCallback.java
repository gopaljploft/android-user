package com.cab_taxi.infinitecabs.NetwrokCheck;

/**
 * Created by robertlevonyan on 11/21/17.
 */

interface ConnectionCallback {
    void hasActiveConnection(boolean hasActiveConnection);
}