package com.cab_taxi.infinitecabs.NetwrokCheck;

import android.app.Dialog;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import com.cab_taxi.infinitecabs.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;

public class NoInternetDialog extends Dialog implements ConnectionListener, ConnectionCallback {

    private ImageView moon;
    private boolean cancelable;
    private WifiReceiver wifiReceiver;
    private NetworkStatusReceiver networkStatusReceiver;
    private ConnectionCallback connectionCallback;

    private NoInternetDialog(@NonNull Context context,
                             boolean cancelable) {
        super(context);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        this.cancelable = cancelable;
        initReceivers(context);
    }

    private void initReceivers(Context context) {
        wifiReceiver = new WifiReceiver();
        context.registerReceiver(wifiReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        networkStatusReceiver = new NetworkStatusReceiver();
        context.registerReceiver(networkStatusReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        wifiReceiver.setConnectionListener(this);
        networkStatusReceiver.setConnectionCallback(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_no_internet);
        initMainWindow();
        initView();
        initClose();
    }

    private void initMainWindow() {
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void initView() {
        moon = findViewById(R.id.moon);
    }

    private void initClose() {
        setCancelable(cancelable);
    }

    @Override
    public void onWifiTurnedOn() {
        getContext().unregisterReceiver(wifiReceiver);
    }

    @Override
    public void onWifiTurnedOff() {
    }

    @Override
    public void hasActiveConnection(boolean hasActiveConnection) {
        if (this.connectionCallback != null)
            this.connectionCallback.hasActiveConnection(hasActiveConnection);
        if (!hasActiveConnection) {
            showDialog();
        } else {
            dismiss();
        }
    }

    @Override
    public void show() {
        super.show();
    }

    public void showDialog() {
        Ping ping = new Ping();
        ping.setConnectionCallback(new ConnectionCallback() {
            @Override
            public void hasActiveConnection(boolean hasActiveConnection) {
                if (!hasActiveConnection) {
                    show();
                }
            }
        });
        ping.execute(getContext());
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }


    public void onDestroy() {
        try {
            getContext().unregisterReceiver(networkStatusReceiver);
        } catch (Exception e) {
        }
        try {
            getContext().unregisterReceiver(wifiReceiver);
        } catch (Exception e) {
        }
    }

    public void setConnectionCallback(ConnectionCallback connectionCallback) {
        this.connectionCallback = connectionCallback;
    }


    public static class Builder {
        private Context context;
        private ConnectionCallback connectionCallback;

        public Builder(Context context) {
            this.context = context;
        }

        public NoInternetDialog build() {
            NoInternetDialog dialog = new NoInternetDialog(context, false);
            dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setConnectionCallback(connectionCallback);
            return dialog;
        }
    }
}
