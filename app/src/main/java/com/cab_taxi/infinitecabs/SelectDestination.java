package com.cab_taxi.infinitecabs;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;
import com.cab_taxi.infinitecabs.Dashboard.Dashboard;
import com.cab_taxi.infinitecabs.Extra.AutoCompleteAdapter;
import com.cab_taxi.infinitecabs.Model.Faviourate_Place_Model;
import com.cab_taxi.infinitecabs.Model.PlacePredictions;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.SQLLite_DataBase.DBManager;
import com.cab_taxi.infinitecabs.SQLLite_DataBase.DataProvider;
import com.cab_taxi.infinitecabs.SQLLite_DataBase.DbHelper;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;
import com.cab_taxi.infinitecabs.Utils.Utilities;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;

import static com.cab_taxi.infinitecabs.DrawerActivities.HomeFragment.MY_PERMISSIONS_REQUEST_LOCATION;

public class SelectDestination extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ActivityCompat.OnRequestPermissionsResultCallback {
    // LogCat tag
    private static final String TAG = SelectDestination.class.getSimpleName();
    private final static int PLAY_SERVICES_REQUEST = 1000;
    private final static int REQUEST_CHECK_SETTINGS = 2000;
    private final String GETPLACESHIT = "places_hit";
    private final PlacePredictions placePredictions = new PlacePredictions();
    Random rnd = new Random();
    FusedLocationProviderClient mFusedLocationClient;
    boolean isPermissionGranted = true;
    double latitude;
    double longitude;
    Activity thisActivity;
    Utilities utils = new Utilities();
    String strSelected = "destination";
    EditText et_destination;
    LinearLayout delete;
    LocationRequest mLocationRequest;
    String id;
    CustomAdapter mCustomAdapter;
    SQLiteDatabase mSQLiteDatabase;
    DbHelper mDbHelper;
    Cursor mCursor;
    Context mContext;
    boolean click = false;
    String savelat;
    String savelng;
    String picup_lat = "";
    String picup_lng = "";
    String dest_lat = "";
    ImageView iv_cross, iv_cross2;
    String dest_lng = "";
    int n = 0;
    LocationManager manager;
    RecyclerView saved_place_recyclerview;
    UserSessionManager session;
    HashMap<String, String> user;
    List<Faviourate_Place_Model.Data> savedlist = new ArrayList<>();
    NoInternetDialog noInternetDialog;
    private EditText et_pickUp_location;
    private AutoCompleteAdapter mAutoCompleteAdapter;
    private ListView mAutoCompleteList;
    private Location mLastLocation;
    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location location = locationList.get(locationList.size() - 1);
                Log.i("MapsActivity", "Location: " + location.getLatitude() + " " + location.getLongitude());
                mLastLocation = location;
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    buildAlertMessageNoGps();
                } else {
                    getLocation();
                }
                if (mLastLocation != null) {
                    latitude = mLastLocation.getLatitude();
                    longitude = mLastLocation.getLongitude();
                    getAddress();

                } else {
                    showToast("Couldn't get the location. Make sure location is enabled on the device");
                }
            }
        }
    };
    private Handler handler;
    // Google client to interact with Google API
    private PlacePredictions predictions = new PlacePredictions();
    private GoogleApiClient mGoogleApiClient;
    private SwipeMenuListView recycler_view_cab_available;
    private DBManager dbManager;
    private SimpleCursorAdapter adapter;
    private String gettingUsernameFromItemClicked;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_destination);
        noInternetDialog = new NoInternetDialog.Builder(SelectDestination.this).build();
        Window window = this.getWindow();
        n = 100000 + rnd.nextInt(900000);
        Log.d("sdf", "" + n);
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.black_text_color));
        mContext = this;
        initView();

        mCustomAdapter = new CustomAdapter(mContext, R.layout.save_address_adapter);


        recycler_view_cab_available.setAdapter(mCustomAdapter);

        getInformation();

        recycler_view_cab_available.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView textview = view.findViewById(R.id.placeposition);
                TextView place_address = view.findViewById(R.id.place_address);
                TextView lat = view.findViewById(R.id.lat);
                TextView lng = view.findViewById(R.id.lng);
                if (strSelected.equals("destination")) {
                    et_destination.setText(place_address.getText().toString());
                    savelat = lat.getText().toString();
                    savelng = lng.getText().toString();

                    dest_lat = lat.getText().toString();
                    dest_lng = lng.getText().toString();
                    Intent intent = new Intent();
                    Log.d("vishal123", intent + "");
                    intent.putExtra("pick_location", et_pickUp_location.getText().toString());
                    intent.putExtra("drop_location", et_destination.getText().toString());
                    intent.putExtra("pic_lat", picup_lat);
                    intent.putExtra("pic_lng", picup_lng);
                    intent.putExtra("drop_lat", dest_lat);
                    intent.putExtra("drop_lng", dest_lng);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    et_pickUp_location.setText(place_address.getText().toString());
                    savelat = lat.getText().toString();
                    savelng = lng.getText().toString();
                    picup_lat = lat.getText().toString();
                    picup_lng = lng.getText().toString();
                }

            }


        });
        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
//                SwipeMenuItem openItem = new SwipeMenuItem(
//                        getApplicationContext());
//                // set item background
//                openItem.setBackground(new ColorDrawable(Color.rgb(0x00, 0x66,
//                        0xff)));
//                // set item width
//                openItem.setWidth(170);
//                // set item title
//                openItem.setTitle("Open");
//                // set item title fontsize
//                openItem.setTitleSize(18);
//                // set item title font color
//                openItem.setTitleColor(Color.WHITE);
//                // add to menu
//                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(170);
                // set a icon

                deleteItem.setIcon(R.drawable.ic_bin);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
        recycler_view_cab_available.setMenuCreator(creator);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }

    public void getInformation() {
        mDbHelper = new DbHelper(getApplicationContext());
        mSQLiteDatabase = mDbHelper.getReadableDatabase();
        mCursor = mDbHelper.getInformation(mSQLiteDatabase);
        if (mCursor.moveToFirst()) {// will return true if information is available false if not
            do {
                String placeposition = mCursor.getString(0);
                String placename = mCursor.getString(1);
                String placeaddress = mCursor.getString(2);
                String lat = mCursor.getString(3);
                String lng = mCursor.getString(4);
                DataProvider dataProvider = new DataProvider(placeposition, placename, placeaddress, lat, lng); // sending to dataprovider class to make into objects
                mCustomAdapter.add(dataProvider);// adding objects
            } while (mCursor.moveToNext()); // all information is stored in mCursor
        }


    }

    public void deleteData() {
        mDbHelper = new DbHelper(getApplicationContext());
        mSQLiteDatabase = mDbHelper.getReadableDatabase();
        String[] whereArgs = new String[]{gettingUsernameFromItemClicked};// to ensure no sql injection
        String whereClause = DbHelper.PLACEPOSITION + " = ?"; // to ensure no sql injection
        mDbHelper.deleteInformation(whereClause, whereArgs, mSQLiteDatabase);
        CustomToast.makeText(getApplication(), DbHelper.PLACEADDRESS + "Place Address Delete Succesfully", CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
        recreate(); // refreshes the activity to show the deleted person
    }

    private void initView() {
        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        thisActivity = this;
        ImageView iv_close = findViewById(R.id.iv_close);
        ImageView iv_current_location = findViewById(R.id.iv_current_location);
        et_pickUp_location = findViewById(R.id.et_pickUp_location);
        saved_place_recyclerview = findViewById(R.id.saved_place_recyclerview);

        iv_cross = findViewById(R.id.iv_cross);
        iv_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_pickUp_location.setText("");
            }
        });
        iv_cross2 = findViewById(R.id.iv_cross2);
        iv_cross2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_destination.setText("");
            }
        });
        recycler_view_cab_available = findViewById(R.id.recycler_view_cab_available);
        et_destination = findViewById(R.id.et_destination);
        CardView card_myfavoutites = findViewById(R.id.card_myfavoutites);
        mAutoCompleteList = findViewById(R.id.searchResultLV);
//        et_pickUp_location.setText(getIntent().getStringExtra("pickup_location"));
        iv_close.setOnClickListener(this);
        iv_current_location.setOnClickListener(this);
        card_myfavoutites.setOnClickListener(this);

        if (checkPlayServices()) {

            // Building the GoogleApi client
            buildGoogleApiClient();
        }

        et_pickUp_location.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    strSelected = "source";
                } else {

                }
            }
        });

        et_destination.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    strSelected = "destination";
                } else {
                }
            }
        });

        et_destination.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                imgDestClose.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // optimised way is to start searching for laction after user has typed minimum 3 chars
               /* imgDestClose.setVisibility(View.VISIBLE);
                strSelected = "destination";*/
                if (et_destination.getText().length() > 0) {
                    iv_cross2.setVisibility(View.VISIBLE);
                    Runnable run = new Runnable() {
                        @Override
                        public void run() {
                            // cancel all the previous requests in the queue to optimise your network calls during autocomplete search
                            TranxitApplication.getInstance().cancelRequestInQueue(GETPLACESHIT);

                            JSONObject object = new JSONObject();
                            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, getPlaceAutoCompleteUrl(et_destination.getText().toString()), object, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.v("PayNowRequestResponse", response.toString());
                                    Log.v("PayNowRequestResponse", response.toString());
                                    Gson gson = new Gson();
                                    predictions = gson.fromJson(response.toString(), PlacePredictions.class);
                                    if (mAutoCompleteAdapter == null) {
                                        mAutoCompleteList.setVisibility(View.VISIBLE);
                                        mAutoCompleteAdapter = new AutoCompleteAdapter(SelectDestination.this, predictions.getPlaces(), SelectDestination.this);
                                        mAutoCompleteList.setAdapter(mAutoCompleteAdapter);
                                    } else {
                                        mAutoCompleteList.setVisibility(View.VISIBLE);
                                        mAutoCompleteAdapter.clear();
                                        mAutoCompleteAdapter.addAll(predictions.getPlaces());
                                        mAutoCompleteAdapter.notifyDataSetChanged();
                                        mAutoCompleteList.invalidate();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.v("PayNowRequestResponse", error.toString());
                                }
                            });
                            TranxitApplication.getInstance().addToRequestQueue(jsonObjectRequest);

                        }

                    };

                    // only canceling the network calls will not help, you need to remove all callbacks as well
                    // otherwise the pending callbacks and messages will again invoke the handler and will send the request
                    if (handler != null) {
                        handler.removeCallbacksAndMessages(null);
                    } else {
                        handler = new Handler();
                    }
                    handler.postDelayed(run, 1000);

                } else {
                    iv_cross2.setVisibility(View.GONE);
//                    lnrFavorite.setVisibility(View.VISIBLE);
                    mAutoCompleteList.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
//                imgDestClose.setVisibility(View.VISIBLE);
            }

        });

        et_pickUp_location.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                imgDestClose.setVisibility(View.VISIBLE);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // optimised way is to start searching for laction after user has typed minimum 3 chars
               /* imgDestClose.setVisibility(View.VISIBLE);
                strSelected = "destination";*/
                if (et_pickUp_location.getText().length() > 0) {
                    iv_cross.setVisibility(View.VISIBLE);
                    iv_current_location.setVisibility(View.GONE);
                    Runnable run = new Runnable() {
                        @Override
                        public void run() {
                            // cancel all the previous requests in the queue to optimise your network calls during autocomplete search
                            TranxitApplication.getInstance().cancelRequestInQueue(GETPLACESHIT);

                            JSONObject object = new JSONObject();
                            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, getPlaceAutoCompleteUrl(et_pickUp_location.getText().toString()), object, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.v("PayNowRequestResponse", response.toString());
                                    Log.v("PayNowRequestResponse", response.toString());
                                    Gson gson = new Gson();
                                    predictions = gson.fromJson(response.toString(), PlacePredictions.class);
                                    if (mAutoCompleteAdapter == null) {
                                        mAutoCompleteList.setVisibility(View.VISIBLE);
                                        mAutoCompleteAdapter = new AutoCompleteAdapter(SelectDestination.this, predictions.getPlaces(), SelectDestination.this);
                                        mAutoCompleteList.setAdapter(mAutoCompleteAdapter);
                                    } else {
                                        mAutoCompleteList.setVisibility(View.VISIBLE);
                                        mAutoCompleteAdapter.clear();
                                        mAutoCompleteAdapter.addAll(predictions.getPlaces());
                                        mAutoCompleteAdapter.notifyDataSetChanged();
                                        mAutoCompleteList.invalidate();


                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.v("PayNowRequestResponse", error.toString());
                                }
                            });
                            TranxitApplication.getInstance().addToRequestQueue(jsonObjectRequest);

                        }

                    };

                    // only canceling the network calls will not help, you need to remove all callbacks as well
                    // otherwise the pending callbacks and messages will again invoke the handler and will send the request
                    if (handler != null) {
                        handler.removeCallbacksAndMessages(null);
                    } else {
                        handler = new Handler();
                    }
                    handler.postDelayed(run, 1000);

                } else {
                    iv_cross.setVisibility(View.GONE);
                    iv_current_location.setVisibility(View.VISIBLE);
//                    lnrFavorite.setVisibility(View.VISIBLE);
                    mAutoCompleteList.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
//                imgDestClose.setVisibility(View.VISIBLE);
            }

        });

        //txtDestination.setText("");

        et_destination.setSelection(et_destination.getText().length());
        mLocationRequest = new LocationRequest();
        mAutoCompleteList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (et_pickUp_location.getText().toString().equalsIgnoreCase("")) {
                    try {
                        AlertDialog.Builder builder = new AlertDialog.Builder(thisActivity);
                        LayoutInflater inflater = (LayoutInflater) thisActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        builder.setMessage("Please choose pickup location")
                                .setTitle(thisActivity.getString(R.string.app_name))
                                .setCancelable(true)
                                .setIcon(R.mipmap.ic_launcher)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        et_pickUp_location.requestFocus();
                                        et_destination.setText("");
//                                        imgDestClose.setVisibility(View.GONE);
                                        mAutoCompleteList.setVisibility(View.GONE);
                                        dialog.dismiss();

                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    setGoogleAddress(position);
                }
            }
        });
        /*getLocation();
        if (mLastLocation != null) {
            latitude = mLastLocation.getLatitude();
            longitude = mLastLocation.getLongitude();
            getAddress();

        } else {

                    *//*if(btn_savePlace.isEnabled())
                        btn_savePlace.setEnabled(false);*//*

            showToast("Couldn't get the location. Make sure location is enabled on the device");
        }*/
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        Map<String, String> map = new HashMap<>();
        //(email,password)
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        Saved_Lists(map);

    }

    private void setGoogleAddress(final int position) {
        if (mGoogleApiClient != null) {
            Utilities.print("", "Place ID == >" + predictions.getPlaces().get(position).getPlaceID());

            Utilities.getAddressUsingPlaceId(SelectDestination.this, predictions.getPlaces().get(position).getPlaceID(), new Utilities.OnPlaceSearch() {
                @Override
                public void onSearch(JSONObject place) {

                    try {
                        if (place.getString("status").equals("OK")) {

                            LatLng latLng = new LatLng(place.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getDouble("lat")
                                    , place.getJSONArray("results").getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getDouble("lng"));

                            if (strSelected.equalsIgnoreCase("destination")) {
                                placePredictions.strDestAddress = place.getJSONArray("results").getJSONObject(0).getString("formatted_address");
                                placePredictions.strDestLatLng = latLng.toString();
                                placePredictions.strDestLatitude = latLng.latitude + "";
                                placePredictions.strDestLongitude = latLng.longitude + "";
                                et_destination.setText(placePredictions.strDestAddress);
                                et_destination.setSelection(0);
                                n = 100000 + rnd.nextInt(900000);
                                dest_lat = placePredictions.strDestLatitude;
                                dest_lng = placePredictions.strDestLongitude;


                                Log.d("vishal123", "dest=" + placePredictions.strDestLatLng + "" + placePredictions.strSourceLongitude);
                                Intent intent = new Intent();
                                Log.d("vishal123", intent + "");
                                intent.putExtra("pick_location", et_pickUp_location.getText().toString());
                                intent.putExtra("drop_location", et_destination.getText().toString());
                                intent.putExtra("pic_lat", picup_lat);
                                intent.putExtra("pic_lng", picup_lng);
                                intent.putExtra("drop_lat", dest_lat);
                                intent.putExtra("drop_lng", dest_lng);
                                setResult(RESULT_OK, intent);
                                finish();
                            } else {
                                placePredictions.strSourceAddress = place.getJSONArray("results").getJSONObject(0).getString("formatted_address");
                                placePredictions.strSourceLatLng = latLng.toString();
                                placePredictions.strSourceLatitude = latLng.latitude + "";
                                placePredictions.strSourceLongitude = latLng.longitude + "";
                                n = 100000 + rnd.nextInt(900000);
                                et_pickUp_location.setText(placePredictions.strSourceAddress);
                                et_pickUp_location.setSelection(0);
                                et_destination.requestFocus();
                                mAutoCompleteAdapter = null;
                                mAutoCompleteList.setVisibility(View.GONE);
                                picup_lat = placePredictions.strSourceLatitude;
                                picup_lng = placePredictions.strSourceLongitude;

                                Log.d("vishal123", "pickup=" + placePredictions.strSourceLatitude + "" + placePredictions.strSourceLongitude);
                                /*Intent intent = new Intent();
                                Log.d("vishal123", intent + "");


                                intent.putExtra("pick_location", et_destination.getText().toString());
                                //intent.putExtra("drop_location", placePredictions.strDestAddress);
                                intent.putExtra("destination", "destination");
                                intent.putExtra("lat",placePredictions.strDestLatitude );
                                intent.putExtra("lng", placePredictions.strDestLatLng);
                                //intent.putExtra("drop_lat", placePredictions.strDestAddress);
                                // intent.putExtra("drop_location", placePredictions.strDestAddress);
                                setResult(RESULT_OK, intent);
                                finish();*/


                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mAutoCompleteList.setVisibility(View.GONE);
                   /* if (et_destination.getText().toString().length() > 0) {
                        if (strSelected.equalsIgnoreCase("destination")) {
                            if (!placePredictions.strDestAddress.equals(placePredictions.strSourceAddress)) {
                                setAddress();
                            } else {
                                utils.showAlert(thisActivity, "Source and Destination address should not be same!");
                            }
                        }
                    } else {
                        et_destination.requestFocus();
                        et_destination.setText("");
//                        imgDestClose.setVisibility(View.GONE);
                        mAutoCompleteList.setVisibility(View.GONE);
                    }*/

                }
            });


//            Places.GeoDataApi.getPlaceById(mGoogleApiClient, predictions.getPlaces().get(position).getPlaceID())
//                    .setResultCallback(new ResultCallback<PlaceBuffer>() {
//                        @Override
//                        public void onResult(PlaceBuffer places) {
//                            if (places.getStatus().isSuccess()) {
//                                Place myPlace = places.get(0);
//                                LatLng queriedLocation = myPlace.getLatLng();
//                                Log.v("Latitude is", "" + queriedLocation.latitude);
//                                Log.v("Longitude is", "" + queriedLocation.longitude);
//                                if (strSelected.equalsIgnoreCase("destination")) {
//                                    placePredictions.strDestAddress = myPlace.getAddress().toString();
//                                    placePredictions.strDestLatLng = myPlace.getLatLng().toString();
//                                    placePredictions.strDestLatitude = myPlace.getLatLng().latitude + "";
//                                    placePredictions.strDestLongitude = myPlace.getLatLng().longitude + "";
//                                    txtDestination.setText(placePredictions.strDestAddress);
//                                    txtDestination.setSelection(0);
//                                } else {
//                                    placePredictions.strSourceAddress = myPlace.getAddress().toString();
//                                    placePredictions.strSourceLatLng = myPlace.getLatLng().toString();
//                                    placePredictions.strSourceLatitude = myPlace.getLatLng().latitude + "";
//                                    placePredictions.strSourceLongitude = myPlace.getLatLng().longitude + "";
//                                    txtaddressSource.setText(placePredictions.strSourceAddress);
//                                    txtaddressSource.setSelection(0);
//                                    txtDestination.requestFocus();
//                                    mAutoCompleteAdapter = null;
//                                }
//                            }
//                            mAutoCompleteList.setVisibility(View.GONE);
//
//                            if (txtDestination.getText().toString().length() > 0) {
//                                places.release();
//                                if (strSelected.equalsIgnoreCase("destination")) {
//                                    if (placePredictions.strDestAddress.equalsIgnoreCase(placePredictions.strSourceAddress)) {
//                                        setAddress();
//                                    } else {
//                                        utils.showAlert(thisActivity, "Source and Destination address should not be same!");
//                                    }
//                                }
//                            } else {
//                                txtDestination.requestFocus();
//                                txtDestination.setText("");
//                                imgDestClose.setVisibility(View.GONE);
//                                mAutoCompleteList.setVisibility(View.GONE);
//                            }
//                        }
//                    });
        }
    }

    public String getPlaceAutoCompleteUrl(String input) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/place/autocomplete/json");
        urlString.append("?input=");
        try {
            urlString.append(URLEncoder.encode(input, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String key  = getResources().getString(R.string.key1)+getResources().getString(R.string.key2)+getResources().getString(R.string.key3);
        urlString.append("&location=");
        urlString.append(latitude + "," + longitude); // append lat long of current location to show nearby results.
        urlString.append("&sessiontoken=" + n);
        urlString.append("&components=country:au");
        urlString.append("&key=" + key);

        Log.d("FINAL URL:::   ", urlString.toString());
        return urlString.toString();
    }

    @Override
    public void onBackPressed() {
        Intent d = new Intent(this, Dashboard.class);
        startActivity(d);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.iv_close:
                onBackPressed();
                break;

            case R.id.iv_current_location:

               /* if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                    buildAlertMessageNoGps();
                }else{
                    getLocation();
                }
*/
                setUserCurrentLocation();
                /*if (mLastLocation != null) {
                    latitude = Double.parseDouble(Utility.getlat(SelectDestination.this));
                    longitude = Double.parseDouble(Utility.getlng(SelectDestination.this));
                    Log.d("asdfasd",latitude+"____"+longitude);
                    getAddress();

                } else {
*//*

                    if(btn_savePlace.isEnabled())
                        btn_savePlace.setEnabled(false);
*//*

                    showToast("Couldn't get the location. Make sure location is enabled on the device");
                }*/

                break;

            case R.id.card_myfavoutites:
                startActivity(new Intent(SelectDestination.this, SaveAddress.class));
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                break;

        }
    }

    private void getLocation() {

        if (isPermissionGranted) {
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessageNoGps();
            } else {
                FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(SelectDestination.this);
                fusedLocationClient.getLastLocation().addOnSuccessListener(this, location -> {
                    if (location != null) {
                        try {
                            mLastLocation = location;
                            latitude = mLastLocation.getLatitude();
                            longitude = mLastLocation.getLongitude();

                            getAddress();
                        } catch (SecurityException e) {
                            e.printStackTrace();
                        }
                    } else {
                        // Handle null case or Request periodic location update https://developer.android.com/training/location/receive-location-updates
                    }
                });
            }
        }

    }

    void setAddress() {
        utils.hideKeypad(thisActivity, getCurrentFocus());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent();
                Log.d("iiiRes...", "" + placePredictions);
                if (placePredictions != null) {
                    intent.putExtra("Location Address", placePredictions);
                    intent.putExtra("pick_location", "no");
                    setResult(RESULT_OK, intent);
                } else {
                    setResult(RESULT_CANCELED, intent);
                }
                finish();
            }
        }, 500);
    }

    public Address getAddress(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            return addresses.get(0);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    public void getAddress() {

        Address locationAddress = getAddress(latitude, longitude);

        if (locationAddress != null) {
            String address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();

            String currentLocation;
            if (address.equals("null")) {
                if (!TextUtils.isEmpty(address)) {
                    currentLocation = address;

                    if (!TextUtils.isEmpty(address1))
                        currentLocation += "\n" + address1;

                    if (!TextUtils.isEmpty(city)) {
                        currentLocation += "\n" + city;

                        if (!TextUtils.isEmpty(postalCode))
                            currentLocation += " - " + postalCode;
                    } else {
                        if (!TextUtils.isEmpty(postalCode))
                            currentLocation += "\n" + postalCode;
                    }

                    if (!TextUtils.isEmpty(state))
                        currentLocation += "\n" + state;

                    if (!TextUtils.isEmpty(country))
                        currentLocation += "\n" + country;
                    picup_lat = latitude + "";
                    picup_lng = longitude + "";
                    currentLocation = getIntent().getStringExtra("pickup_location");
                    et_pickUp_location.setText(currentLocation);
                    et_pickUp_location.setVisibility(View.VISIBLE);

                    Log.d("asdfasd", latitude + "_11___" + longitude + "__" + currentLocation);
                /*if(!btn_savePlace.isEnabled())
                    btn_savePlace.setEnabled(true);
*/

                }
            } else {
                picup_lat = latitude + "";
                picup_lng = longitude + "";
                currentLocation = getIntent().getStringExtra("pickup_location");
                currentLocation = address;
                et_pickUp_location.setText(currentLocation);
                Log.d("asdfasd", latitude + "_1133" +
                        "___" + longitude + "__" + currentLocation);
                et_pickUp_location.setVisibility(View.VISIBLE);
            }


        }

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        mGoogleApiClient.connect();

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {

                final Status status = locationSettingsResult.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location requests here
//                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(SelectDestination.this, REQUEST_CHECK_SETTINGS);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });


    }

    private boolean checkPlayServices() {

        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();

        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(resultCode)) {
                googleApiAvailability.getErrorDialog(this, resultCode,
                        PLAY_SERVICES_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
//                        getLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        break;
                    default:
                        break;
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Once connected with google api, get the location
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            getLocation();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + connectionResult.getErrorCode());

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(SelectDestination.this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
//                        googleMap.setMyLocationEnabled(true);
                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
//                    Toast.makeText(getActivity(), "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @SuppressLint("SetTextI18n")
    private void setUserCurrentLocation() {
        Location location = Utilities.getLocation(this);
        if (location != null) {
            Address address = Utilities.getAddressUsingLatLang(this, location.getLatitude(), location.getLongitude());
            if (address != null) {
                et_pickUp_location.setText(address.getAddressLine(0));
            }
        } else {
            fetchLocation();
        }
    }

    private void fetchLocation() {
        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    Address address = Utilities.getAddressUsingLatLang(SelectDestination.this, location.getLatitude(), location.getLongitude());
                    if (address != null) {
                        et_pickUp_location.setText(address.getAddressLine(0));
                    }
                }
            }
        });
    }

    private void Saved_Lists(Map<String, String> map) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Faviourate_Place_Model> call = Apis.getAPIService().getFavouritePlace(map);
        call.enqueue(new Callback<Faviourate_Place_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<Faviourate_Place_Model> call, retrofit2.Response<Faviourate_Place_Model> response) {
                dialog.dismiss();
                Faviourate_Place_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {
                        savedlist.clear();
                        savedlist = userdata.getData();
                        if (savedlist.size() > 0) {
                            saved_place_recyclerview.setVisibility(View.VISIBLE);
                            saved_place_recyclerview.setAdapter(new Saved_ListAdapter());
                        } else {
                            saved_place_recyclerview.setVisibility(View.GONE);
                        }

                    } else {
                        saved_place_recyclerview.setVisibility(View.GONE);
//                        swipeToRefresh.setVisibility(View.GONE);
                        // CustomToast.makeText(getActivity(), userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<Faviourate_Place_Model> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    private void Delete_address(Map<String, String> map) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Faviourate_Place_Model> call = Apis.getAPIService().Delete_address(map);
        call.enqueue(new Callback<Faviourate_Place_Model>() {
            @SuppressLint("Assert")
            @Override
            public void onResponse(Call<Faviourate_Place_Model> call, retrofit2.Response<Faviourate_Place_Model> response) {
                dialog.dismiss();
                Faviourate_Place_Model userdata = response.body();
                if (userdata != null) {
                    if (userdata.getStatusCode().equals("200")) {
                        Map<String, String> map = new HashMap<>();
                        //(email,password)
                        map.put("user_id", user.get(UserSessionManager.KEY_ID));
                        Saved_Lists(map);
                    } else {

//                        swipeToRefresh.setVisibility(View.GONE);
                        // CustomToast.makeText(getActivity(), userdata.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                    }

                }
            }

            @Override
            public void onFailure(Call<Faviourate_Place_Model> call, Throwable t) {
                dialog.dismiss();
                Log.d("response", "vv" + t.getMessage());
            }
        });
    }

    public class CustomAdapter extends ArrayAdapter {

        List mList = new ArrayList(); // list variable for items


        public CustomAdapter(Context context, int resource) {
            // default constructor
            super(context, resource);


        }

        @Override
        public int getCount() {
            return mList.size();
        }

        @Nullable
        @Override
        public Object getItem(int position) {
            return mList.get(position);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            View row = convertView;
            LayoutHandler layoutHandler;
            DataProvider dataProvider = (DataProvider) this.getItem(position);
            if (row == null) {
                LayoutInflater layoutInflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = layoutInflater.inflate(R.layout.save_address_adapter, parent, false);
                layoutHandler = new LayoutHandler();
                layoutHandler.placename = row.findViewById(R.id.place_name);
                layoutHandler.lat = row.findViewById(R.id.lat);
                layoutHandler.lng = row.findViewById(R.id.lng);
                // layoutHandler.delete = (LinearLayout) row.findViewById(R.id.delete);
                layoutHandler.placeposition = row.findViewById(R.id.placeposition);
                layoutHandler.descTextView = row.findViewById(R.id.place_address);

                row.setTag(layoutHandler);
                recycler_view_cab_available.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                        switch (index) {
                            case 0:
                                Log.d(TAG, "onMenuItemClick: clicked item " + index);
                                gettingUsernameFromItemClicked = dataProvider.getPlaceposition();
                                // String text = (String)mListView.getItemAtPosition(i); does not work due to dataprovider dont know why
                                deleteData();
                                Toast.makeText(mContext, " " + gettingUsernameFromItemClicked, Toast.LENGTH_SHORT).show();
                                break;
                            case 1:
                                Log.d(TAG, "onMenuItemClick: clicked item " + index);
                                break;
                        }
                        // false : close the menu; true : not close the menu
                        return false;
                    }
                });
            } else {
                layoutHandler = (LayoutHandler) row.getTag();
            }

            layoutHandler.placeposition.setText(dataProvider.getPlaceposition());
            layoutHandler.descTextView.setText(dataProvider.getPlaceaddress());
            layoutHandler.lat.setText(dataProvider.getLat());
            layoutHandler.lng.setText(dataProvider.getLng());


            return row;

        }

        @Override
        public void add(@Nullable Object object) {
            super.add(object);
            mList.add(object);
        }

        class LayoutHandler {
            TextView placename, lat, lng, placeposition, descTextView;
            LinearLayout delete;// declaring the textviews to use within the list
        }

    }

    public class Saved_ListAdapter extends RecyclerView.Adapter<Saved_ListAdapter.MyViewHolder> {


        public Saved_ListAdapter() {
        }

        @Override
        public Saved_ListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.save_address_adapter, parent, false);

            return new Saved_ListAdapter.MyViewHolder(itemView);

        }

        @Override
        public void onBindViewHolder(@NonNull final Saved_ListAdapter.MyViewHolder holder, final int i) {
            holder.placeposition.setText(savedlist.get(i).getType());
            holder.place_address.setText(savedlist.get(i).getLocations());
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Map<String, String> map = new HashMap<>();
                    map.put("id", savedlist.get(i).getId());
                    Delete_address(map);
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (strSelected.equals("destination")) {
                        if (et_pickUp_location.getText().length() > 0) {
                            et_destination.setText(savedlist.get(i).getLocations());
                            savelat = savedlist.get(i).getLat();
                            savelng = savedlist.get(i).getLng();
                            dest_lat = savedlist.get(i).getLat();
                            dest_lng = savedlist.get(i).getLng();
                            Intent intent = new Intent();
                            Log.d("vishal123", intent + "");
                            intent.putExtra("pick_location", et_pickUp_location.getText().toString());
                            intent.putExtra("drop_location", et_destination.getText().toString());
                            intent.putExtra("pic_lat", picup_lat);
                            intent.putExtra("pic_lng", picup_lng);
                            intent.putExtra("drop_lat", dest_lat);
                            intent.putExtra("drop_lng", dest_lng);
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            Toast.makeText(SelectDestination.this, "Please choose the pickup location first", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        et_pickUp_location.setText(savedlist.get(i).getLocations());
                        savelat = savedlist.get(i).getLat();
                        savelng = savedlist.get(i).getLng();
                        picup_lat = savedlist.get(i).getLat();
                        picup_lng = savedlist.get(i).getLng();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return savedlist.size();
        }


        public long getItemId(int position) {
            return position;
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            private final TextView placeposition;
            private final TextView place_address;
            private final ImageView delete;

            public MyViewHolder(View v) {
                super(v);
                this.place_address = itemView.findViewById(R.id.place_address);
                this.placeposition = itemView.findViewById(R.id.placeposition);
                this.delete = itemView.findViewById(R.id.delete);
            }
        }

    }

}

