package com.cab_taxi.infinitecabs.SQLLite_DataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Arrays;

/**
 * Created by chris on 3/7/2017.
 */

public class DbHelper extends SQLiteOpenHelper {
    private static final String TAG = "DATABASE_OPERATION";

    private static final String DATABASE_NAME = "SAVEADDRESS_TAXIAPP";
    private static final int DATABASE_VERSION =1;
    public static final String SCORE_TABLE_NAME = "SAVE_ADDRESS"; // key used to represent the column

    public static final String PLACEPOSITION = "place_postion";
    public static final String PLACENAME = "place_name";
    public static final String PLACEADDRESS = "place_address";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    private Context context;

    private static final String CREATE_QUERY = "CREATE TABLE "+ SCORE_TABLE_NAME+"("+ PLACEPOSITION+" Text,"
            + PLACENAME+" Text,"+ PLACEADDRESS+" Text,"+ LAT+" Text,"+ LNG+" Text);"; // pay attention to spacing carefully

    public DbHelper(Context context){//constructor
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
        Log.e(TAG,"DATABASE CREATED/OPENED");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) { // create table is only called the first time once the table is created it is not called

        sqLiteDatabase.execSQL(CREATE_QUERY);
        Log.e(TAG,"Table Created");
    }

    public void addInformation(String placeposition, String placename,String placeaddress,String lat,String lng , SQLiteDatabase db ){

        ContentValues contentValues = new ContentValues();
        contentValues.put(DatabaseHelper.PLACEPOSITION, placeposition);
        contentValues.put(DatabaseHelper.PLACENAME, placename);
        contentValues.put(DatabaseHelper.PLACEADDRESS, placeaddress);
        contentValues.put(DatabaseHelper.LAT, lat);
        contentValues.put(DatabaseHelper.LNG, lng);
        db.insert(SCORE_TABLE_NAME, null , contentValues);
       // CustomToast.makeText(context, "Save Address Insert Successfully", CustomToast.LENGTH_SHORT, CustomToast.SUCCESS,true).show();

        Log.e(TAG,"One row is inserted");
    }

    public Cursor getInformation(SQLiteDatabase db){

        Cursor cursor;// objects that retrieves information from the database
        String[] projections = {PLACEPOSITION,PLACENAME,PLACEADDRESS,LAT,LNG};
        String orderBy = PLACEPOSITION+" DESC";
        String limit = "5";
        

        cursor = db.query(SCORE_TABLE_NAME,projections,null,null,null,null,orderBy,limit); // the nulls have to do with where clause info

        return cursor;
    }

    public void deleteInformation(String whereClause , String[] whereArgs, SQLiteDatabase db){


        db.delete(SCORE_TABLE_NAME,whereClause,whereArgs);


        Log.e(TAG,"One row is deleted with username: "+ Arrays.toString(whereArgs));
    }




    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}