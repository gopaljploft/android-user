package com.cab_taxi.infinitecabs.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Searching_Driver_Model {

    @Expose
    @SerializedName("bookingData")
    private BookingData bookingData;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("statusCode")
    private String statusCode;

    public BookingData getBookingData() {
        return bookingData;
    }

    public void setBookingData(BookingData bookingData) {
        this.bookingData = bookingData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public static class BookingData {
        @Expose
        @SerializedName("status")
        private String status;
        @Expose
        @SerializedName("userstarrating")
        private String userstarrating;
        @Expose
        @SerializedName("droplng")
        private String droplng;
        @Expose
        @SerializedName("droplat")
        private String droplat;
        @Expose
        @SerializedName("pickuplng")
        private String pickuplng;
        @Expose
        @SerializedName("pickuplat")
        private String pickuplat;
        @Expose
        @SerializedName("bookingdate")
        private String bookingdate;
        @Expose
        @SerializedName("driverImage")
        private String driverImage;
        @Expose
        @SerializedName("driverCar_Make")
        private String driverCar_Make;
        @Expose
        @SerializedName("driverCar_Model")
        private String driverCar_Model;
        @Expose
        @SerializedName("drivercar_type")
        private String drivercar_type;
        @Expose
        @SerializedName("driverlicense_no")
        private String driverlicense_no;
        @Expose
        @SerializedName("driverMobile")
        private String driverMobile;
        @Expose
        @SerializedName("driverEmail")
        private String driverEmail;
        @Expose
        @SerializedName("driverName")
        private String driverName;
        @Expose
        @SerializedName("driverId")
        private String driverId;
        @Expose
        @SerializedName("dropLocation")
        private String dropLocation;
        @Expose
        @SerializedName("pickupLocation")
        private String pickupLocation;
        @Expose
        @SerializedName("bookingId")
        private String bookingId;
        @Expose
        @SerializedName("drivercar_no")
        private String drivercar_no;

        public String getDrivercar_no() {
            return drivercar_no;
        }

        public void setDrivercar_no(String drivercar_no) {
            this.drivercar_no = drivercar_no;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getUserstarrating() {
            return userstarrating;
        }

        public void setUserstarrating(String userstarrating) {
            this.userstarrating = userstarrating;
        }

        public String getDroplng() {
            return droplng;
        }

        public void setDroplng(String droplng) {
            this.droplng = droplng;
        }

        public String getDroplat() {
            return droplat;
        }

        public void setDroplat(String droplat) {
            this.droplat = droplat;
        }

        public String getPickuplng() {
            return pickuplng;
        }

        public void setPickuplng(String pickuplng) {
            this.pickuplng = pickuplng;
        }

        public String getPickuplat() {
            return pickuplat;
        }

        public void setPickuplat(String pickuplat) {
            this.pickuplat = pickuplat;
        }

        public String getBookingdate() {
            return bookingdate;
        }

        public void setBookingdate(String bookingdate) {
            this.bookingdate = bookingdate;
        }

        public String getDriverImage() {
            return driverImage;
        }

        public void setDriverImage(String driverImage) {
            this.driverImage = driverImage;
        }

        public String getDriverCar_Make() {
            return driverCar_Make;
        }

        public void setDriverCar_Make(String driverCar_Make) {
            this.driverCar_Make = driverCar_Make;
        }

        public String getDriverCar_Model() {
            return driverCar_Model;
        }

        public void setDriverCar_Model(String driverCar_Model) {
            this.driverCar_Model = driverCar_Model;
        }

        public String getDrivercar_type() {
            return drivercar_type;
        }

        public void setDrivercar_type(String drivercar_type) {
            this.drivercar_type = drivercar_type;
        }

        public String getDriverlicense_no() {
            return driverlicense_no;
        }

        public void setDriverlicense_no(String driverlicense_no) {
            this.driverlicense_no = driverlicense_no;
        }

        public String getDriverMobile() {
            return driverMobile;
        }

        public void setDriverMobile(String driverMobile) {
            this.driverMobile = driverMobile;
        }

        public String getDriverEmail() {
            return driverEmail;
        }

        public void setDriverEmail(String driverEmail) {
            this.driverEmail = driverEmail;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getDropLocation() {
            return dropLocation;
        }

        public void setDropLocation(String dropLocation) {
            this.dropLocation = dropLocation;
        }

        public String getPickupLocation() {
            return pickupLocation;
        }

        public void setPickupLocation(String pickupLocation) {
            this.pickupLocation = pickupLocation;
        }

        public String getBookingId() {
            return bookingId;
        }

        public void setBookingId(String bookingId) {
            this.bookingId = bookingId;
        }
    }
}
