package com.cab_taxi.infinitecabs.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserUpcommingRideModel {
    @Expose
    @SerializedName("data")
    private List<Data> data;
    @Expose
    @SerializedName("image_path")
    private String image_path;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("statusCode")
    private String statusCode;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public static class Data {
        @Expose
        @SerializedName("book_create_date_time")
        private String book_create_date_time;
        @Expose
        @SerializedName("user_drop_long_latitude")
        private String user_drop_long_latitude;
        @Expose
        @SerializedName("user_drop_latitude")
        private String user_drop_latitude;
        @Expose
        @SerializedName("user_pickup_long_latitude")
        private String user_pickup_long_latitude;
        @Expose
        @SerializedName("user_pickup_latitude")
        private String user_pickup_latitude;
        @Expose
        @SerializedName("car_type")
        private String car_type;
        @Expose
        @SerializedName("drop_address")
        private String drop_address;
        @Expose
        @SerializedName("amount")
        private String amount;
        @Expose
        @SerializedName("drop_area")
        private String drop_area;
        @Expose
        @SerializedName("pickup_area")
        private String pickup_area;
        @Expose
        @SerializedName("booking_id")
        private String booking_id;
        @Expose
        @SerializedName("book_scheduled_time")
        private String book_scheduled_time;

        public String getBook_scheduled_time() {
            return book_scheduled_time;
        }

        public void setBook_scheduled_time(String book_scheduled_time) {
            this.book_scheduled_time = book_scheduled_time;
        }

        public String getBook_create_date_time() {
            return book_create_date_time;
        }

        public void setBook_create_date_time(String book_create_date_time) {
            this.book_create_date_time = book_create_date_time;
        }

        public String getUser_drop_long_latitude() {
            return user_drop_long_latitude;
        }

        public void setUser_drop_long_latitude(String user_drop_long_latitude) {
            this.user_drop_long_latitude = user_drop_long_latitude;
        }

        public String getUser_drop_latitude() {
            return user_drop_latitude;
        }

        public void setUser_drop_latitude(String user_drop_latitude) {
            this.user_drop_latitude = user_drop_latitude;
        }

        public String getUser_pickup_long_latitude() {
            return user_pickup_long_latitude;
        }

        public void setUser_pickup_long_latitude(String user_pickup_long_latitude) {
            this.user_pickup_long_latitude = user_pickup_long_latitude;
        }

        public String getUser_pickup_latitude() {
            return user_pickup_latitude;
        }

        public void setUser_pickup_latitude(String user_pickup_latitude) {
            this.user_pickup_latitude = user_pickup_latitude;
        }

        public String getCar_type() {
            return car_type;
        }

        public void setCar_type(String car_type) {
            this.car_type = car_type;
        }

        public String getDrop_address() {
            return drop_address;
        }

        public void setDrop_address(String drop_address) {
            this.drop_address = drop_address;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getDrop_area() {
            return drop_area;
        }

        public void setDrop_area(String drop_area) {
            this.drop_area = drop_area;
        }

        public String getPickup_area() {
            return pickup_area;
        }

        public void setPickup_area(String pickup_area) {
            this.pickup_area = pickup_area;
        }

        public String getBooking_id() {
            return booking_id;
        }

        public void setBooking_id(String booking_id) {
            this.booking_id = booking_id;
        }
    }
}
