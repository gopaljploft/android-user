package com.cab_taxi.infinitecabs;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.cab_taxi.infinitecabs.Api.Apis;
import com.cab_taxi.infinitecabs.Custom_Toast.CustomToast;
import com.cab_taxi.infinitecabs.Dashboard.Dashboard;
import com.cab_taxi.infinitecabs.Model.Save_address_Model;
import com.cab_taxi.infinitecabs.NetwrokCheck.NoInternetDialog;
import com.cab_taxi.infinitecabs.PlaceSearch.MapsActivity;
import com.cab_taxi.infinitecabs.SQLLite_DataBase.DBManager;
import com.cab_taxi.infinitecabs.SQLLite_DataBase.DbHelper;
import com.cab_taxi.infinitecabs.Session_Manager.UserSessionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SaveAddress extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ActivityCompat.OnRequestPermissionsResultCallback {

    // LogCat tag
    private static final String TAG = SaveAddress.class.getSimpleName();
    private final static int PLAY_SERVICES_REQUEST = 1000;
    private final static int REQUEST_CHECK_SETTINGS = 2000;
    boolean isPermissionGranted = true;
    DbHelper mDbHelper;
    SQLiteDatabase mSQLiteDatabase;
    double latitude;
    double longitude;
    String place_postion;
    NoInternetDialog noInternetDialog;
    int LAUNCH_SECOND_ACTIVITY = 123;
    private Button btn_home;
    private Button btn_work;
    private Button btn_other;
    private EditText et_place_name;
    // Google client to interact with Google API
    private EditText et_place_address;
    private Button btn_savePlace;
    private Button btn_pickLocation;
    private DBManager dbManager;
   /* // list of permissions

    ArrayList<String> permissions=new ArrayList<>();
    PermissionUtils permissionUtils;

    boolean isPermissionGranted;*/
    private Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    UserSessionManager session;
    HashMap<String, String> user;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_address);
        noInternetDialog = new NoInternetDialog.Builder(SaveAddress.this).build();
        Window window = this.getWindow();
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.black_text_color));
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        noInternetDialog.onDestroy();
    }

    private void initView() {
        session = new UserSessionManager(getApplicationContext());
        user = session.getUserDetails();
        ImageView iv_close = findViewById(R.id.iv_close);
        btn_home = findViewById(R.id.btn_home);
        btn_work = findViewById(R.id.btn_work);
        btn_other = findViewById(R.id.btn_other);
        et_place_name = findViewById(R.id.et_place_name);
        et_place_address = findViewById(R.id.et_place_address);
        btn_pickLocation = findViewById(R.id.btn_pickLocation);
        btn_savePlace = findViewById(R.id.btn_savePlace);
        CardView card_picklocation = findViewById(R.id.card_picklocation);
        iv_close.setOnClickListener(this);
        btn_home.setOnClickListener(this);
        btn_work.setOnClickListener(this);
        btn_other.setOnClickListener(this);
        btn_savePlace.setOnClickListener(this);
        btn_pickLocation.setOnClickListener(this);
        et_place_address.setOnClickListener(this);
        card_picklocation.setOnClickListener(this);
        dbManager = new DBManager(this);
        dbManager.open();

        // check availability of play services
        if (checkPlayServices()) {

            // Building the GoogleApi client
            buildGoogleApiClient();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.et_place_address:
                Intent i = new Intent(this, MapsActivity.class);
                startActivityForResult(i, LAUNCH_SECOND_ACTIVITY);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                break;
            case R.id.iv_close:
                onBackPressed();
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                break;
            case R.id.btn_home:
                place_postion = "Home";
                et_place_name.setText("Home");
                //botton background change
                btn_home.setBackground(getResources().getDrawable(R.drawable.btn_background1));
                btn_work.setBackground(getResources().getDrawable(R.drawable.btn_background));
                btn_other.setBackground(getResources().getDrawable(R.drawable.btn_background));

                //button text change
                btn_home.setTextColor(getResources().getColor(R.color.colorAccent));
                btn_work.setTextColor(getResources().getColor(R.color.colorBlack));
                btn_other.setTextColor(getResources().getColor(R.color.colorBlack));

                break;
            case R.id.btn_work:
                place_postion = "Work";
                et_place_name.setText("Work");
                //botton background change
                btn_home.setBackground(getResources().getDrawable(R.drawable.btn_background));
                btn_work.setBackground(getResources().getDrawable(R.drawable.btn_background1));
                btn_other.setBackground(getResources().getDrawable(R.drawable.btn_background));

                //button text change
                btn_home.setTextColor(getResources().getColor(R.color.colorBlack));
                btn_work.setTextColor(getResources().getColor(R.color.colorAccent));
                btn_other.setTextColor(getResources().getColor(R.color.colorBlack));

                break;
            case R.id.btn_other:
                //botton background change
                place_postion = "Other";
                et_place_name.setText("Other");
                btn_home.setBackground(getResources().getDrawable(R.drawable.btn_background));
                btn_work.setBackground(getResources().getDrawable(R.drawable.btn_background));
                btn_other.setBackground(getResources().getDrawable(R.drawable.btn_background1));

                //button text change
                btn_home.setTextColor(getResources().getColor(R.color.colorBlack));
                btn_work.setTextColor(getResources().getColor(R.color.colorBlack));
                btn_other.setTextColor(getResources().getColor(R.color.colorAccent));

                break;

            case R.id.btn_pickLocation:
                getLocation();
                if (mLastLocation != null) {
                    latitude = mLastLocation.getLatitude();
                    longitude = mLastLocation.getLongitude();
                    getAddress();

                } else {

                    if (btn_savePlace.isEnabled())
                        btn_savePlace.setEnabled(false);

                    showToast("Couldn't get the location. Make sure location is enabled on the device");
                }
                break;

            case R.id.btn_savePlace:
                submit();
                break;


        }
    }

    private void submit() {
        // validate
        String name = et_place_name.getText().toString().trim();
        place_postion = name;
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, "Place name", Toast.LENGTH_SHORT).show();
            return;
        }

        String address = et_place_address.getText().toString().trim();
        if (TextUtils.isEmpty(address)) {
            Toast.makeText(this, "Place address", Toast.LENGTH_SHORT).show();
            return;
        }

//        startActivity(new Intent(SaveAddress.this, Dashboard.class));
//        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);

        Map<String, String> map = new HashMap<>();
        map.put("userId", user.get(UserSessionManager.KEY_ID));
        map.put("place_postion", place_postion);
        map.put("type", name);
        map.put("locations", address);
        map.put("lat",String.valueOf(latitude));
        map.put("lng", String.valueOf(longitude));
        Save_address(map);

        //  dbManager.insert(place_postion,name, address,String.valueOf(latitude),String.valueOf(longitude));
//        sendInformationToSQLDatabase();
    }

    public void sendInformationToSQLDatabase() {
        mDbHelper = new DbHelper(getApplicationContext());
        mSQLiteDatabase = mDbHelper.getWritableDatabase();
        mDbHelper.addInformation(place_postion, et_place_name.getText().toString().trim(), et_place_address.getText().toString().trim(), latitude + "", longitude + "", mSQLiteDatabase);
        mDbHelper.close();
        CustomToast.makeText(getApplication(), "Data Save Succesfully", CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();
        startActivity(new Intent(getApplication(), Dashboard.class));

    }

    private void getLocation() {
        if (isPermissionGranted) {
            try {
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                latitude = mLastLocation.getLatitude();
                longitude = mLastLocation.getLongitude();
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }

    }

    public Address getAddress(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            return addresses.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public void getAddress() {
        Address locationAddress = getAddress(latitude, longitude);
        Log.d("asdfasdf",latitude+"____"+longitude);
        if (locationAddress != null) {
            String address = locationAddress.getAddressLine(0);
            String address1 = locationAddress.getAddressLine(1);
            String city = locationAddress.getLocality();
            String state = locationAddress.getAdminArea();
            String country = locationAddress.getCountryName();
            String postalCode = locationAddress.getPostalCode();
            Log.d("asdfasdf",postalCode+"______");
            String currentLocation;

            if (!TextUtils.isEmpty(address)) {
                currentLocation = address;

                if (!TextUtils.isEmpty(address1))
                    currentLocation += "\n" + address1;

                if (!TextUtils.isEmpty(city)) {
                    currentLocation += "\n" + city;

                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += " - " + postalCode;
                } else {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += "\n" + postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    currentLocation += "\n" + state;

                if (!TextUtils.isEmpty(country))
                    currentLocation += "\n" + country;
                Log.d("asdfasdf",latitude+"____"+longitude+"___"+currentLocation);
                et_place_address.setText(currentLocation);
                et_place_address.setVisibility(View.VISIBLE);

                if (!btn_savePlace.isEnabled())
                    btn_savePlace.setEnabled(true);
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();

        mGoogleApiClient.connect();

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {

                final Status status = locationSettingsResult.getStatus();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location requests here
                        getLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(SaveAddress.this, REQUEST_CHECK_SETTINGS);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });


    }

    private boolean checkPlayServices() {

        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();

        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(resultCode)) {
                googleApiAvailability.getErrorDialog(this, resultCode,
                        PLAY_SERVICES_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 123) {
//                Toast.makeText(EditFeeds.this, data.getStringExtra("address"), Toast.LENGTH_SHORT);
                Log.d("dklf", Objects.requireNonNull(data.getStringExtra("Lng")));
                Log.d("dklf", Objects.requireNonNull(data.getStringExtra("Lat")));
                et_place_address.setText(data.getStringExtra("address"));

                latitude = Double.parseDouble(data.getStringExtra("Lat"));
                longitude = Double.parseDouble(data.getStringExtra("Lng"));
//
//                Log.d("dklf", Objects.requireNonNull(data.getParcelableExtra("Lng")));
            }
        }

        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        getLocation();
                        if (requestCode == 123) {
//                          Toast.makeText(SaveAddress.this, data.getStringExtra("address"), Toast.LENGTH_SHORT);
                            Log.d("dklf", Objects.requireNonNull(data.getStringExtra("Lng")));
                            Log.d("dklf", Objects.requireNonNull(data.getStringExtra("Lat")));
                            et_place_address.setText(data.getStringExtra("address"));
                            latitude = Double.parseDouble(data.getStringExtra("Lat"));
                            longitude = Double.parseDouble(data.getStringExtra("Lng"));
//                            address = data.getStringExtra("address");
//                Log.d("dklf", Objects.requireNonNull(data.getParcelableExtra("Lng")));
                        }

                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        break;
                    case 123:
                        // The user was asked to change settings, but chose not to
                        break;

                    default:
                        break;
                }
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
    }

    public void onConnectionFailed(ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {

        // Once connected with google api, get the location
        getLocation();
    }

    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }


    // Permission check functions


/*    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        // redirects to utils
        permissionUtils.onRequestPermissionsResult(requestCode,permissions,grantResults);

    }*/


    /*public void PermissionGranted(int request_code) {
          Log.i("PERMISSION","GRANTED");
          isPermissionGranted=true;
      }


      public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
          Log.i("PERMISSION PARTIALLY","GRANTED");
      }


      public void PermissionDenied(int request_code) {
          Log.i("PERMISSION","DENIED");
      }


      public void NeverAskAgain(int request_code) {
          Log.i("PERMISSION","NEVER ASK AGAIN");
      }*/
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void Save_address(final Map<String, String> map) {
        final Dialog dialog = new Dialog(SaveAddress.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_login);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Call<Save_address_Model> call = Apis.getAPIService().saveaddress_data(map);
        call.enqueue(new Callback<Save_address_Model>() {
            @Override
            public void onResponse(Call<Save_address_Model> call, Response<Save_address_Model> response) {
//           CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Save_address_Model user = response.body();
                if (user != null) {
                    if (user.getStatusCode().equals("200")) {
                        startActivity(new Intent(SaveAddress.this, Dashboard.class));
                        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                        CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.SUCCESS, true).show();

                    } else {
                        CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();
                    }

                } else {

                    CustomToast.makeText(getApplicationContext(), user.getMessage(), CustomToast.LENGTH_SHORT, CustomToast.ERROR, true).show();

                }
            }

            @Override
            public void onFailure(Call<Save_address_Model> call, Throwable t) {
//        CustomDialog.hideProgressDialog(dialog);
                dialog.dismiss();
                Log.d("response", "gh" + t.getMessage());
            }
        });
    }
}
